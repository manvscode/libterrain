/*
 * Copyright (C) 2013 Joseph A. Marrero.   http://www.manvscode.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <simplegl.h>
#include <libimageio/imageio.h>
#include <m3d/quat.h>
#include <SDL2/SDL.h>
#if DARWIN
# include <OpenGL/gl3.h>
# include <OpenGL/gl3ext.h>
#else
# include <GL/gl.h>
# include <GL/glext.h>
#endif
#include "../src/terrain.h"

#define  MAX_XCHUNKS        4
#define  MAX_YCHUNKS        4
#define  MAX_XCELLS         (MAX_XCHUNKS * 3)
#define  MAX_YCELLS         (MAX_YCHUNKS * 3)
#define  MAX_CELLS          (MAX_XCELLS * MAX_YCELLS)

static void initialize             ( void );
static void deinitialize           ( void );
static void init_terrain           ( bool scale_uvs );
static void render                 ( void );
static void render_terrain         ( void );
static void render_coordinate_axes ( void );
static void dump_sdl_error         ( void );
static void set_view_by_mouse      ( void );



static const uint8_t tile_state[ 3 ][ MAX_YCELLS * MAX_XCELLS] = {
	{
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
	},
};


static SDL_Window* window     = NULL;
static SDL_GLContext ctx      = NULL;
static uint32_t delta         = 0;
const float mouse_sensitivity = 0.001f;

static axes_3d_t axes;

struct {
	GLuint program;
	GLint attribute_vertex;
	GLint attribute_normal;
	GLint attribute_tex_coord;
	GLint attribute_color;
	GLint uniform_projection;
	GLint uniform_model;
	GLint uniform_texture;
	GLint uniform_normal_matrix;
	GLint uniform_light_direction;
	GLint uniform_light_intensity;
	GLint uniform_render_grid;
	GLuint texture;
} terrain_shader = {
	.program = 0,
	.attribute_vertex = 0,
	.attribute_normal = 0,
	.attribute_tex_coord = 0,
	.attribute_color = 0,
	.uniform_projection = 0,
	.uniform_model = 0,
	.uniform_texture = 0,
	.uniform_normal_matrix = 0,
	.uniform_light_direction = 0,
	.uniform_light_intensity = 0,
	.uniform_render_grid = 0,
	.texture = 0
};

static GLuint vao_terrain                    = 0;
static GLuint vbo_terrain_mesh               = 0;
static GLuint vbo_terrain_cells[ MAX_CELLS ] = { 0 };
static camera_t* camera = NULL;
static raster_font_t* font1 = NULL;
static raster_font_t* font2 = NULL;
static terrain_alphamap_t* alphamap = NULL;

#define ASSERT(condition)     GL_ASSERT_NO_ERROR(); assert( condition )

static terrain_mesh_t* terrain_mesh = NULL;
static const terrain_grid_t* terrain_grid = NULL;
static texture_splatted_terrain_t* splat_terrain = NULL;
static vec3_t light_direction = VEC3( 30.0f, 100.0f, -70.0f );
static vec3_t light_intensity = VEC3( 0.8f, 0.8f, 0.8f );



int main( int argc, char* argv[] )
{
	if( SDL_Init(SDL_INIT_VIDEO) < 0 )
	{
		goto quit;
	}

	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 3 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 2 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );

	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
	SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 24 );
	SDL_GL_SetAttribute( SDL_GL_MULTISAMPLEBUFFERS, 1 );
	SDL_GL_SetAttribute( SDL_GL_MULTISAMPLESAMPLES, 4 );

	int flags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;
	//flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
	window = SDL_CreateWindow( "Grid Terrain", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, flags );

	if( window == NULL )
	{
		dump_sdl_error( );
		goto quit;
	}

	ctx = SDL_GL_CreateContext( window );

	if( !ctx )
	{
		dump_sdl_error( );
		goto quit;
	}

	initialize( );

	bool done = false;
	bool fullscreen = false;

	SDL_SetWindowFullscreen( window, fullscreen ? SDL_WINDOW_FULLSCREEN : 0 );
	SDL_ShowCursor( SDL_DISABLE );

	while( !done )
	{
		SDL_PumpEvents();
		const Uint8 *state = SDL_GetKeyboardState(NULL);

		if( state[SDL_SCANCODE_ESCAPE] || state[SDL_SCANCODE_Q] )
		{
			done = true;
		}
		if( state[SDL_SCANCODE_F] )
		{
			fullscreen ^= true;
			SDL_SetWindowFullscreen( window, fullscreen ? SDL_WINDOW_FULLSCREEN : 0 );
			SDL_ShowCursor( fullscreen ? SDL_DISABLE : SDL_ENABLE );
		}
		if( state[SDL_SCANCODE_W] )
		{
			camera_move_forwards( camera, 0.5f );
		}
		if( state[SDL_SCANCODE_S] )
		{
			camera_move_forwards( camera, -0.5f );
		}
		if( state[SDL_SCANCODE_A] )
		{
			camera_move_sideways( camera, 0.5f );
		}
		if( state[SDL_SCANCODE_D] )
		{
			camera_move_sideways( camera, -0.5f );
		}

		render( );
        set_view_by_mouse( );
	}

	deinitialize( );

quit:
	if( ctx ) SDL_GL_DeleteContext( ctx );
	if( window ) SDL_DestroyWindow( window );
	SDL_Quit( );
	return 0;
}

void initialize( void )
{
	gl_info_print( );
	glClearColor( 0.2f, 0.2f, 0.2f, 1.0f );
	glClearDepth( 1.0f );

	glEnable( GL_DEPTH_TEST );
	//glEnable( GL_CULL_FACE );
	glDisable( GL_CULL_FACE );
	glCullFace( GL_BACK );
	glFrontFace( GL_CCW );
	assert( gl_error() == GL_NO_ERROR );

	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	glEnable( GL_LINE_SMOOTH );
	glEnable( GL_POLYGON_SMOOTH );

	//glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

	vec3_normalize( &light_direction );
	vec3_normalize( &light_intensity );

	font2 = raster_font_create( RASTER_FONT_FONT3_16X16 );
	if( !font2 )
	{
		printf( "Unable to create raster font.\n" );
		exit( EXIT_FAILURE );
	}

	font1 = raster_font_create( RASTER_FONT_FONT1_8X8 );
	if( !font1 )
	{
		printf( "Unable to create raster font.\n" );
		exit( EXIT_FAILURE );
	}


	const bool scale_uvs = false;

	init_terrain( scale_uvs );


	const bool vertices_not_tri_stripped = false;

	fractal_terrain_gen_t* terrain_gen = fractal_terrain_gen_create( 300, 300, -10.0f, 30.0f, 10.0f, 40 );
	fractal_terrain_gen_generate( terrain_gen, FTG_DIAMOND_SQUARE, scale_uvs ? 1 : 14, vertices_not_tri_stripped );
	terrain_vertex_t* geometry = fractal_terrain_gen_geometry( terrain_gen );
	size_t geometry_count = fractal_terrain_gen_geometry_size( terrain_gen );

	terrain_mesh = terrain_mesh_create( );
	terrain_mesh_set_geometry( terrain_mesh, geometry, geometry_count, fractal_terrain_gen_element_width(terrain_gen), fractal_terrain_gen_element_length(terrain_gen), vertices_not_tri_stripped );
	fractal_terrain_gen_destroy( terrain_gen );


	splat_terrain = texture_splatted_terrain_create( terrain_mesh, MAX_XCHUNKS, MAX_YCHUNKS);
	terrain_grid = texture_splatted_terrain_grid( splat_terrain );


	/* Terrain VAO */
	{
		glGenVertexArrays( 1, &vao_terrain );  ASSERT( vao_terrain > 0 );
		glBindVertexArray( vao_terrain );

		bool buffer_created = buffer_create( &vbo_terrain_mesh, terrain_mesh_vertices(terrain_mesh), sizeof(terrain_vertex_t), terrain_mesh_vertex_count(terrain_mesh), GL_ARRAY_BUFFER, GL_STATIC_DRAW );
		assert( buffer_created );

		for( size_t y = 0; y < MAX_YCELLS; y++ )
		{
			for( size_t x = 0; x < MAX_XCELLS; x++ )
			{
				const terrain_cell_t* terrain_cell = terrain_grid_cell( terrain_grid, x, y );
				const uint32_t* terrain_indices    = terrain_cell_vertex_indices( terrain_cell );
				size_t terrain_index_count         = terrain_cell_vertex_index_count( terrain_cell );

				buffer_created = buffer_create( &vbo_terrain_cells[ y * MAX_XCELLS + x ], terrain_indices, sizeof(uint32_t), terrain_index_count, GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW );
				assert( buffer_created );
			}
		}

		glEnableVertexAttribArray( terrain_shader.attribute_vertex );
		glVertexAttribPointer( terrain_shader.attribute_vertex, 3, GL_FLOAT, GL_FALSE, sizeof(terrain_vertex_t), (const void*) offsetof(terrain_vertex_t, position) );

		glEnableVertexAttribArray( terrain_shader.attribute_normal );
		glVertexAttribPointer( terrain_shader.attribute_normal, 3, GL_FLOAT, GL_FALSE, sizeof(terrain_vertex_t), (const void*) offsetof(terrain_vertex_t, normal) );

		glEnableVertexAttribArray( terrain_shader.attribute_tex_coord );
		glVertexAttribPointer( terrain_shader.attribute_tex_coord, 2, GL_FLOAT, GL_FALSE, sizeof(terrain_vertex_t), (const void*) offsetof(terrain_vertex_t, texture) );

		#ifdef TERRAIN_VERTEX_HAS_COLOR
		glEnableVertexAttribArray( terrain_shader.attribute_color );
		glVertexAttribPointer( terrain_shader.attribute_color, 3, GL_FLOAT, GL_FALSE, sizeof(terrain_vertex_t), (const void*) offsetof(terrain_vertex_t, color) );
		#endif
		glBindVertexArray( 0 );
	}

	assert( gl_error() == GL_NO_ERROR );

	axes = axes_3d_create( 1.0f );
	assert( gl_error() == GL_NO_ERROR );

	int width; int height;
	SDL_GetWindowSize( window, &width, &height );
	ASSERT( width > 0 && height > 0 );

	SDL_WarpMouseInWindow( window, width / 2, height / 2 );

	#if 0
	camera = camera_create( );
	camera_set_position( camera, &VEC3(0.0f, 80.0f, 0.0f) );
	float aspect = ((float)height) / width;
	camera_set_orthographic( camera, -100.0, 100.0, -100.0 * aspect, 100.0 * aspect, -1000.0f, 1000.0 );
	#else
	camera = camera_create( );
	camera_set_position( camera, &VEC3(0.0f, 80.0f, 0.0f) );
	camera_set_perspective_for_viewport( camera, width, height, 2.0f, 1000.0f, 45.0f );
	#endif

	const char* alphamap_file = "./assets/splat-transitions.alp";
	alphamap = terrain_alphamap_from_file( alphamap_file );
	if( !alphamap )
	{
		printf( "Generating terrain splat transitions...\n" );
		alphamap = terrain_alphamap_create( 128, 128, 1, false, 1.0f );
		printf( "Saving terrain splat transitions...\n" );
		terrain_alphamap_save_file( alphamap, alphamap_file );
	}
}

void deinitialize( void )
{
	if( splat_terrain ) texture_splatted_terrain_destroy( splat_terrain );
	if( terrain_mesh ) terrain_mesh_destroy( terrain_mesh );
	axes_3d_destroy( &axes );
	tex_destroy( terrain_shader.texture );
	glDeleteBuffers( 1, &vbo_terrain_mesh );
	glDeleteBuffers( MAX_CELLS, vbo_terrain_cells );
	glDeleteVertexArrays( 1, &vao_terrain );
	glDeleteProgram( terrain_shader.program );
	camera_destroy( camera );
	raster_font_destroy( font1 );
	raster_font_destroy( font2 );
	terrain_alphamap_destroy( alphamap );
}

void render( )
{
	uint32_t now = SDL_GetTicks( );
	delta = frame_delta( now /* milliseconds */ );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );

	camera_update( camera, delta );


	int width; int height;
	SDL_GetWindowSize( window, &width, &height );
	ASSERT( width > 0 && height > 0 );


	glViewport( 0, 0, width, height );
	glScissor( 0, 0, width, height );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );
	assert( gl_error() == GL_NO_ERROR );

	render_terrain( );






	render_coordinate_axes( );

	glViewport(0, 0, width, height );

	raster_font_shadowed_writef( font2, &VEC2(3, 1 + 8 * 1.5f ), &VEC3(1,1,0), &VEC3_ZERO, 0.92f, "Grid Terrain Demo" );

	vec3_t forward = camera_forward_vector( camera );
	raster_font_shadowed_writef( font1, &VEC2(3, 1), &VEC3(1,1,1), &VEC3_ZERO, 1.0f, "FPS: %.1f, Direction: (%.1f, %.1f, %.1f)", frame_rate(delta), forward.x, forward.y, forward.z );

	raster_font_shadowed_writef( font1, &VEC2(861, 1), &VEC3(0,1,1), &VEC3_ZERO, 1.0f, "Coded by Joe Marrero" );

	SDL_GL_SwapWindow( window );
	assert( gl_error() == GL_NO_ERROR );
}

void render_coordinate_axes( void )
{
	int screen_width; int screen_height;
	SDL_GetWindowSize( window, &screen_width, &screen_height );
	ASSERT( screen_width > 0 && screen_height > 0 );

	const int size = 50;
	glViewport( screen_width - size, 0, size, size );
	glScissor( screen_width - size, 0, size, size );

	const mat4_t* orientation = camera_orientation_matrix( camera );
	mat4_t transform = m3d_translate( &VEC3( 0.0f, 0.0f, -3.0f ) );
	transform = mat4_mult_matrix( &transform, orientation  );
	const mat4_t* projection = camera_projection_matrix( camera );
	mat4_t model_view = mat4_mult_matrix( projection, &transform );

	glDepthMask( GL_FALSE );
	axes_3d_render( axes, model_view.m );
	glDepthMask( GL_TRUE );
}

void dump_sdl_error( void )
{
	const char* sdl_error = SDL_GetError( );

	if( sdl_error && *sdl_error != '\0' )
	{
		fprintf( stderr, "[SDL] %s\n", sdl_error );
	}
}

void init_terrain( bool scale_uvs )
{
	GLchar* shader_log  = NULL;
	GLchar* program_log = NULL;
	const shader_info_t shaders[] = {
		{ GL_VERTEX_SHADER,   "./assets/shaders/texture-splatted-terrain.v.glsl" },
		{ GL_FRAGMENT_SHADER, "./assets/shaders/texture-splatted-terrain.f.glsl" }
	};

	if( !glsl_program_from_shaders( &terrain_shader.program, shaders, shader_info_count(shaders), &shader_log, &program_log ) )
	{
		assert( gl_error() == GL_NO_ERROR );

		if( shader_log )
		{
			printf( " [Shader Log] %s\n", shader_log );
			free( shader_log );
		}
		if( program_log )
		{
			printf( "[Program Log] %s\n", program_log );
			free( program_log );
		}

		printf( "Shaders did not compile and link!!\n" );
		exit( EXIT_FAILURE );
	}

	terrain_shader.attribute_vertex        = glsl_bind_attribute( terrain_shader.program, "a_vertex" );        ASSERT( terrain_shader.attribute_vertex >= 0 );
	terrain_shader.attribute_normal        = glsl_bind_attribute( terrain_shader.program, "a_normal" );        ASSERT( terrain_shader.attribute_normal >= 0 );
	terrain_shader.attribute_tex_coord     = glsl_bind_attribute( terrain_shader.program, "a_tex_coord" );     ASSERT( terrain_shader.attribute_tex_coord >= 0 );
	terrain_shader.attribute_color         = glsl_bind_attribute( terrain_shader.program, "a_color" );
	terrain_shader.uniform_projection      = glsl_bind_uniform( terrain_shader.program, "u_projection" );      ASSERT( terrain_shader.uniform_projection >= 0 );
	terrain_shader.uniform_model           = glsl_bind_uniform( terrain_shader.program, "u_model" );           ASSERT( terrain_shader.uniform_model >= 0 );
	terrain_shader.uniform_texture         = glsl_bind_uniform( terrain_shader.program, "u_texture" );         ASSERT( terrain_shader.uniform_texture >= 0 );
	terrain_shader.uniform_normal_matrix   = glsl_bind_uniform( terrain_shader.program, "u_normal_matrix" );   ASSERT( terrain_shader.uniform_normal_matrix >= 0 );
	terrain_shader.uniform_light_direction = glsl_bind_uniform( terrain_shader.program, "u_light_direction" ); ASSERT( terrain_shader.uniform_light_direction >= 0 );
	terrain_shader.uniform_light_intensity = glsl_bind_uniform( terrain_shader.program, "u_light_intensity" ); ASSERT( terrain_shader.uniform_light_intensity >= 0 );
	terrain_shader.uniform_render_grid     = glsl_bind_uniform( terrain_shader.program, "u_render_grid" );     ASSERT( terrain_shader.uniform_render_grid >= 0 );

	terrain_shader.texture = tex_create( );
	assert( gl_error() == GL_NO_ERROR );


	if( terrain_shader.texture )
	{
		glActiveTexture( GL_TEXTURE0 );
		GLubyte texture_flags = scale_uvs ? (TEX_CLAMP_S | TEX_CLAMP_T) : 0;

		if( scale_uvs )
		{
			if( !tex_load_2d_with_linear( terrain_shader.texture, "./assets/textures/terrain-texture.tga", texture_flags ) )
			{
				fprintf( stderr, "Unable to load texture.\n" );
				exit( EXIT_FAILURE );
			}
		}
		else
		{
			if( !tex_load_2d( terrain_shader.texture, "./assets/textures/debug.png", GL_NEAREST, GL_NEAREST, texture_flags ) )
			//if( !tex_load_2d_with_mipmaps( terrain_shader.texture, "./assets/textures/grass03.png", texture_flags ) )
			{
				fprintf( stderr, "Unable to load texture.\n" );
				exit( EXIT_FAILURE );
			}
		}

		//glTexParameterf( texture, GL_TEXTURE_MAX_ANISOTROPY_EXT, 5.0f );
		assert( gl_error() == GL_NO_ERROR );
	}
}


void render_terrain( void )
{
	const mat4_t* projection = camera_projection_matrix(camera);
	const mat4_t* model      = camera_model_matrix( camera );

	glUseProgram( terrain_shader.program );
	assert( gl_error() == GL_NO_ERROR );
	glUniformMatrix4fv( terrain_shader.uniform_projection, 1, GL_FALSE, projection->m );
	assert( gl_error() == GL_NO_ERROR );
	glUniformMatrix4fv( terrain_shader.uniform_model, 1, GL_FALSE, model->m );
	assert( gl_error() == GL_NO_ERROR );

	const mat3_t normal_matrix = camera_normal_matrix( camera );

	glUniformMatrix3fv( terrain_shader.uniform_normal_matrix, 1, GL_FALSE, normal_matrix.m );
	assert( gl_error() == GL_NO_ERROR );

	glUniform3fv( terrain_shader.uniform_light_direction, 1, (float*) &light_direction );
	assert( gl_error() == GL_NO_ERROR );
	glUniform3fv( terrain_shader.uniform_light_intensity, 1, (float*) &light_intensity );
	assert( gl_error() == GL_NO_ERROR );

	/* Render grid outlines for debugging. */
	glUniform1i( terrain_shader.uniform_render_grid, 0 );
	assert( gl_error() == GL_NO_ERROR );


	glBindVertexArray( vao_terrain );
	glActiveTexture( GL_TEXTURE0 );
	glBindTexture( GL_TEXTURE_2D, terrain_shader.texture );
	glUniform1i( terrain_shader.uniform_texture, 0 );
	assert( gl_error() == GL_NO_ERROR );

	glPointSize( 2.0f );

	for( size_t y = 0; y < MAX_YCELLS; y++ )
	{
		for( size_t x = 0; x < MAX_XCELLS; x++ )
		{
			if ( x == y ) continue;
			if ( MAX_XCELLS - 1 - x == y ) continue;

			const terrain_cell_t* terrain_cell = terrain_grid_cell( terrain_grid, x, y );
			size_t terrain_index_count         = terrain_cell_vertex_index_count( terrain_cell );
			//const uint32_t* terrain_indices    = terrain_cell_vertex_indices( terrain_cell );

			glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, vbo_terrain_cells[ y * MAX_XCELLS + x ] );
			glDrawElements( GL_TRIANGLE_STRIP, terrain_index_count, GL_UNSIGNED_INT, NULL );
			assert( gl_error() == GL_NO_ERROR );
		}
	}
	assert( gl_error() == GL_NO_ERROR );
	glBindVertexArray( 0 );
}

void set_view_by_mouse( )
{
	int screen_width;
	int screen_height;
	SDL_GetWindowSize( window, &screen_width, &screen_height );


	int middle_x = screen_width / 2; // the middle of the screen in the x direction
	int middle_y = screen_height / 2; // the middle of the screen in the y direction


	// the coordinates of our mouse coordinates
	int mouse_x;
	int mouse_y;
	// This function gets the position of the mouse
	SDL_GetMouseState( &mouse_x, &mouse_y );

	// if the mouse hasn't moved, return without doing
	// anything to our view
	if (mouse_x == middle_x && mouse_y == middle_y ) return;

	// otherwise move the mouse back to the middle of the screen
	SDL_WarpMouseInWindow( window, middle_x, middle_y );

	// get the distance and direction the mouse moved in x (in
	// pixels). We can't use the actual number of pixels in radians,
	// as only six pixels  would cause a full 360 degree rotation.
	// So we use a mousesensitivity variable that can be changed to
	// vary how many radians we want to turn in the x-direction for
	// a given mouse movement distance

	// We have to remember that positive rotation is counter-clockwise.
	// Moving the mouse down is a negative rotation about the x axis
	// Moving the mouse right is a negative rotation about the y axis
	// vector that describes mouseposition - center
	vec2_t mouse_direction = VEC2(
        (mouse_x - middle_x) * mouse_sensitivity,
		(mouse_y - middle_y) * mouse_sensitivity
	);

    if( scaler_abs(mouse_direction.x) > m3d_to_radians(0.05f) || scaler_abs(mouse_direction.y) > m3d_to_radians(0.05f) )
    {
        camera_offset_orientation( camera, mouse_direction.x, mouse_direction.y );
    }
}
