/*
 * Copyright (C) 2013 Joseph A. Marrero.   http://www.manvscode.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <simplegl.h>
#include <libimageio/imageio.h>
#include <m3d/quat.h>
#include <SDL2/SDL.h>
#if DARWIN
# include <OpenGL/gl3.h>
# include <OpenGL/gl3ext.h>
#else
# include <GL/gl.h>
# include <GL/glext.h>
#endif
#include "../src/terrain.h"

static void initialize             ( void );
static void deinitialize           ( void );
static void init_terrain           ( void );
static void init_skybox            ( void );
static void render                 ( void );
static void render_terrain         ( void );
static void render_coordinate_axes ( void );
static void render_skybox          ( void );
static void dump_sdl_error         ( void );
static void set_view_by_mouse      ( void );

static SDL_Window* window     = NULL;
static SDL_GLContext ctx      = NULL;
static uint32_t delta         = 0;
const float mouse_sensitivity = 0.001f;

static fractal_terrain_gen_t* terrain_gen = NULL;
static axes_3d_t axes;

struct {
	GLuint program;
	GLint attribute_vertex;
	GLint attribute_normal;
	GLint attribute_tex_coord;
	#ifdef TERRAIN_VERTEX_HAS_COLOR
	GLint attribute_color;
	#else
	GLint uniform_color;
	#endif
	GLint uniform_projection;
	GLint uniform_model;
	GLint uniform_texture;
	GLint uniform_normal_matrix;
	GLint uniform_light_direction;
	GLint uniform_light_intensity;
	GLint uniform_render_grid;
} terrain_shader;

struct {
	GLuint program;
	GLint attribute_vertex;
	GLint uniform_projection;
	GLint uniform_orientation;
	GLint uniform_cubemap;
} skybox_shader;


static GLuint terrain_vao     = 0;
static GLuint terrain_vbo     = 0;
static GLuint terrain_texture = 0;
static GLuint skybox_vao      = 0;
static GLuint skybox_vbo      = 0;
static GLuint skybox_texture  = 0;
static camera_t* camera = NULL;
static raster_font_t* font1 = NULL;
static raster_font_t* font2 = NULL;

#define ASSERT(condition)     GL_ASSERT_NO_ERROR(); assert( condition )

static vec3_t light_direction = VEC3( -400.0f, 1000.0f, 120.0f );
static vec3_t light_intensity = VEC3( 0.8f, 0.8f, 0.8f );
static const GLfloat skybox_vertices[] = {
	-10.0f,-10.0f,-10.0f,
	-10.0f,-10.0f, 10.0f,
	-10.0f, 10.0f, 10.0f,
	 10.0f, 10.0f,-10.0f,
	-10.0f,-10.0f,-10.0f,
	-10.0f, 10.0f,-10.0f,
	 10.0f,-10.0f, 10.0f,
	-10.0f,-10.0f,-10.0f,
	 10.0f,-10.0f,-10.0f,
	 10.0f, 10.0f,-10.0f,
	 10.0f,-10.0f,-10.0f,
	-10.0f,-10.0f,-10.0f,
	-10.0f,-10.0f,-10.0f,
	-10.0f, 10.0f, 10.0f,
	-10.0f, 10.0f,-10.0f,
	 10.0f,-10.0f, 10.0f,
	-10.0f,-10.0f, 10.0f,
	-10.0f,-10.0f,-10.0f,
	-10.0f, 10.0f, 10.0f,
	-10.0f,-10.0f, 10.0f,
	 10.0f,-10.0f, 10.0f,
	 10.0f, 10.0f, 10.0f,
	 10.0f,-10.0f,-10.0f,
	 10.0f, 10.0f,-10.0f,
	 10.0f,-10.0f,-10.0f,
	 10.0f, 10.0f, 10.0f,
	 10.0f,-10.0f, 10.0f,
	 10.0f, 10.0f, 10.0f,
	 10.0f, 10.0f,-10.0f,
	-10.0f, 10.0f,-10.0f,
	 10.0f, 10.0f, 10.0f,
	-10.0f, 10.0f,-10.0f,
	-10.0f, 10.0f, 10.0f,
	 10.0f, 10.0f, 10.0f,
	-10.0f, 10.0f, 10.0f,
 	 10.0f,-10.0f, 10.0f
};

const bool random_push = false;
const bool tri_strip   = true;
const bool scale_uvs   = false;


int main( int argc, char* argv[] )
{
	if( SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0 )
	{
		goto quit;
	}

	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 3 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 2 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );

	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
	SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 8 );
	SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 8 );
	SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 8 );
	SDL_GL_SetAttribute( SDL_GL_ALPHA_SIZE, 8 );
	SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 8 );
	SDL_GL_SetAttribute( SDL_GL_MULTISAMPLEBUFFERS, 1 );
	SDL_GL_SetAttribute( SDL_GL_MULTISAMPLESAMPLES, 8 );

	int flags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;
	window = SDL_CreateWindow( "Fractal Terrain Generator", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1024, 768, flags );

	if( window == NULL )
	{
		dump_sdl_error( );
		goto quit;
	}

	ctx = SDL_GL_CreateContext( window );
	SDL_GL_SetSwapInterval( 1 ); /* vsync */

	if( !ctx )
	{
		dump_sdl_error( );
		goto quit;
	}

	initialize( );

	bool done = false;
	bool fullscreen = false;

	SDL_SetWindowFullscreen( window, fullscreen ? SDL_WINDOW_FULLSCREEN : 0 );
	SDL_ShowCursor( SDL_DISABLE );

	while( !done )
	{
		SDL_PumpEvents();
		const Uint8 *state = SDL_GetKeyboardState(NULL);

		if( state[SDL_SCANCODE_ESCAPE] || state[SDL_SCANCODE_Q] )
		{
			done = true;
		}
		if( state[SDL_SCANCODE_F] )
		{
			fullscreen ^= true;
			SDL_SetWindowFullscreen( window, fullscreen ? SDL_WINDOW_FULLSCREEN : 0 );
			SDL_ShowCursor( fullscreen ? SDL_DISABLE : SDL_ENABLE );
		}
		if( state[SDL_SCANCODE_W] )
		{
			camera_move_forwards( camera, 0.5f );
		}
		if( state[SDL_SCANCODE_S] )
		{
			camera_move_forwards( camera, -0.5f );
		}
		if( state[SDL_SCANCODE_A] )
		{
			camera_move_sideways( camera, 0.5f );
		}
		if( state[SDL_SCANCODE_D] )
		{
			camera_move_sideways( camera, -0.5f );
		}

		render( );
        set_view_by_mouse( );
	}

	deinitialize( );

quit:
	if( ctx ) SDL_GL_DeleteContext( ctx );
	if( window ) SDL_DestroyWindow( window );
	SDL_Quit( );
	return 0;
}

void initialize( void )
{
	gl_info_print( );

	memset( &terrain_shader, 0, sizeof(terrain_shader) );
	memset( &skybox_shader, 0, sizeof(skybox_shader) );

	glClearColor( 0.2f, 0.2f, 0.2f, 0.0f );
	glClearDepth( 1.0f );

	glEnable( GL_DEPTH_TEST );
	glEnable( GL_CULL_FACE );
	//glDisable( GL_CULL_FACE );
	glCullFace( GL_BACK );
	glFrontFace( GL_CCW );
	assert( gl_error() == GL_NO_ERROR );

	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	glPointSize( 5.0f );
	//glEnable( GL_POINT_SMOOTH );
	glEnable( GL_LINE_SMOOTH );
	glEnable( GL_POLYGON_SMOOTH );
	glEnable( GL_TEXTURE_CUBE_MAP_SEAMLESS );
	//glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

	//glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
	assert(gl_error() == GL_NO_ERROR);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	assert(gl_error() == GL_NO_ERROR);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
	assert(gl_error() == GL_NO_ERROR);

	vec3_normalize( &light_direction );
	vec3_normalize( &light_intensity );

	font2 = raster_font_create( RASTER_FONT_FONT3_16X16 );
	if( !font2 )
	{
		printf( "Unable to create raster font.\n" );
		exit( EXIT_FAILURE );
	}

	font1 = raster_font_create( RASTER_FONT_FONT1_8X8 );
	if( !font1 )
	{
		printf( "Unable to create raster font.\n" );
		exit( EXIT_FAILURE );
	}

	init_skybox( );
	init_terrain( );

	axes = axes_3d_create( 1.0f );
	assert( gl_error() == GL_NO_ERROR );

	int width; int height;
	SDL_GetWindowSize( window, &width, &height );
	ASSERT( width > 0 && height > 0 );

	SDL_WarpMouseInWindow( window, width / 2, height / 2 );
	camera = camera_perspective_create( width, height, 2.0f, 1000.0f, 45.0f, &VEC3(0.0f, 40.0f, 0.0f) );

	assert( gl_error() == GL_NO_ERROR );
}

void deinitialize( void )
{
	axes_3d_destroy( &axes );
	tex_destroy( terrain_texture );
	glDeleteBuffers( 1, &terrain_vbo );
	glDeleteVertexArrays( 1, &terrain_vao );
	glDeleteBuffers( 1, &skybox_vbo );
	glDeleteVertexArrays( 1, &skybox_vao );
	glDeleteProgram( terrain_shader.program );
	glDeleteProgram( skybox_shader.program );
	fractal_terrain_gen_destroy( terrain_gen );
	camera_destroy( camera );
	raster_font_destroy( font1 );
	raster_font_destroy( font2 );
}

void init_terrain( void )
{
	GLchar* shader_log  = NULL;
	GLchar* program_log = NULL;

	{
		const shader_info_t shaders[] = {
			{ GL_VERTEX_SHADER,   "./assets/shaders/terrain.v.glsl" },
			{ GL_FRAGMENT_SHADER, "./assets/shaders/terrain.f.glsl" }
		};

		if( !glsl_program_from_shaders( &terrain_shader.program, shaders, shader_info_count(shaders), &shader_log, &program_log ) )
		{
			assert( gl_error() == GL_NO_ERROR );
			if( shader_log )
			{
				printf( " [Shader Log] %s\n", shader_log );
				free( shader_log );
			}
			if( program_log )
			{
				printf( "[Program Log] %s\n", program_log );
				free( program_log );
			}

			printf( "Shaders did not compile and link!!\n" );
			exit( EXIT_FAILURE );
		}

		terrain_shader.attribute_vertex        = glsl_bind_attribute( terrain_shader.program, "a_vertex" );    ASSERT( terrain_shader.attribute_vertex >= 0 );
		terrain_shader.attribute_normal        = glsl_bind_attribute( terrain_shader.program, "a_normal" );    ASSERT( terrain_shader.attribute_normal >= 0 );
		terrain_shader.attribute_tex_coord     = glsl_bind_attribute( terrain_shader.program, "a_tex_coord" ); ASSERT( terrain_shader.attribute_tex_coord >= 0 );
		#ifdef TERRAIN_VERTEX_HAS_COLOR
		terrain_shader.attribute_color         = glsl_bind_attribute( terrain_shader.program, "a_color" );
		#else
		terrain_shader.uniform_color           = glsl_bind_uniform( terrain_shader.program, "u_color" );
		#endif
		terrain_shader.uniform_projection      = glsl_bind_uniform( terrain_shader.program, "u_projection" );  ASSERT( terrain_shader.uniform_projection >= 0 );
		terrain_shader.uniform_model           = glsl_bind_uniform( terrain_shader.program, "u_model" );       ASSERT( terrain_shader.uniform_model >= 0 );
		terrain_shader.uniform_texture         = glsl_bind_uniform( terrain_shader.program, "u_texture" );
		terrain_shader.uniform_normal_matrix   = glsl_bind_uniform( terrain_shader.program, "u_normal_matrix" );       ASSERT( terrain_shader.uniform_normal_matrix >= 0 );
		terrain_shader.uniform_light_direction = glsl_bind_uniform( terrain_shader.program, "u_light_direction" );     ASSERT( terrain_shader.uniform_light_direction >= 0 );
		terrain_shader.uniform_light_intensity = glsl_bind_uniform( terrain_shader.program, "u_light_intensity" );     ASSERT( terrain_shader.uniform_light_intensity >= 0 );
		terrain_shader.uniform_render_grid     = glsl_bind_uniform( terrain_shader.program, "u_render_grid" );   ASSERT( terrain_shader.uniform_render_grid >= 0 );
		assert( gl_error() == GL_NO_ERROR );
	}

	terrain_texture = tex_create( );
	assert( gl_error() == GL_NO_ERROR );

	if( terrain_texture )
	{
		glActiveTexture( GL_TEXTURE0 );
		assert( gl_error() == GL_NO_ERROR );
		GLubyte texture_flags = scale_uvs ? (TEX_CLAMP_S | TEX_CLAMP_T) : 0;
		if( !tex_load_2d_with_mipmaps( terrain_texture, "./assets/textures/mars.png", texture_flags ) )
		//if( !tex_load_2d_with_mipmaps( terrain_texture, "./assets/textures/grass03.png", texture_flags ) )
		{
			fprintf( stderr, "Unable to load texture.\n" );
			exit( EXIT_FAILURE );
		}
		assert( gl_error() == GL_NO_ERROR );
	}
	else
	{
		dump_sdl_error( );
	}

	/* Generate terrain and create VAO and VBOs */
	{
		if( random_push )
		{
			terrain_gen = fractal_terrain_gen_create( 260.0f, 260.0f, -30.0f, 40.0f, 22.0f, 40 );
			fractal_terrain_gen_generate( terrain_gen, FTG_RANDOMIZED_PUSH, scale_uvs ? 1 : 12, tri_strip );
		}
		else
		{
			terrain_gen = fractal_terrain_gen_create( 300, 300, -20.0f, 40.0f, 20.0f, 40 );
			fractal_terrain_gen_generate( terrain_gen, FTG_DIAMOND_SQUARE, scale_uvs ? 1 : 12, tri_strip );
		}


		glGenVertexArrays( 1, &terrain_vao );  ASSERT( terrain_vao > 0 );
		glBindVertexArray( terrain_vao );

		terrain_vertex_t* geometry = fractal_terrain_gen_geometry( terrain_gen );
		size_t size = fractal_terrain_gen_geometry_size( terrain_gen );

		bool buffer_created = buffer_create( &terrain_vbo, geometry, sizeof(terrain_vertex_t), size, GL_ARRAY_BUFFER, GL_STATIC_DRAW );
		assert( buffer_created );

		glEnableVertexAttribArray( terrain_shader.attribute_vertex );
		glVertexAttribPointer( terrain_shader.attribute_vertex, 3, GL_FLOAT, GL_FALSE, sizeof(terrain_vertex_t), (const void*) offsetof(terrain_vertex_t, position) );

		glEnableVertexAttribArray( terrain_shader.attribute_normal );
		glVertexAttribPointer( terrain_shader.attribute_normal, 3, GL_FLOAT, GL_FALSE, sizeof(terrain_vertex_t), (const void*) offsetof(terrain_vertex_t, normal) );

		glEnableVertexAttribArray( terrain_shader.attribute_tex_coord );
		glVertexAttribPointer( terrain_shader.attribute_tex_coord, 2, GL_FLOAT, GL_FALSE, sizeof(terrain_vertex_t), (const void*) offsetof(terrain_vertex_t, texture) );

		#ifdef TERRAIN_VERTEX_HAS_COLOR
		glEnableVertexAttribArray( terrain_shader.attribute_color );
		glVertexAttribPointer( terrain_shader.attribute_color, 3, GL_FLOAT, GL_FALSE, sizeof(terrain_vertex_t), (const void*) offsetof(terrain_vertex_t, color) );
		#endif

		glBindVertexArray( 0 );
		assert( gl_error() == GL_NO_ERROR );
	}

}

void init_skybox( void )
{
	GLchar* shader_log  = NULL;
	GLchar* program_log = NULL;

	{
		const shader_info_t shaders[] = {
			{ GL_VERTEX_SHADER,   "./assets/shaders/skybox.v.glsl" },
			{ GL_FRAGMENT_SHADER, "./assets/shaders/skybox.f.glsl" }
		};

		if( !glsl_program_from_shaders( &skybox_shader.program, shaders, shader_info_count(shaders), &shader_log, &program_log ) )
		{
			assert( gl_error() == GL_NO_ERROR );
			if( shader_log )
			{
				printf( " [Shader Log] %s\n", shader_log );
				free( shader_log );
			}
			if( program_log )
			{
				printf( "[Program Log] %s\n", program_log );
				free( program_log );
			}

			printf( "Shaders did not compile and link!!\n" );
			exit( EXIT_FAILURE );
		}

		skybox_shader.attribute_vertex    = glsl_bind_attribute( skybox_shader.program, "a_vertex" );    ASSERT( skybox_shader.attribute_vertex >= 0 );
		skybox_shader.uniform_projection  = glsl_bind_uniform( skybox_shader.program, "u_projection" );  ASSERT( skybox_shader.uniform_projection >= 0 );
	    skybox_shader.uniform_orientation = glsl_bind_uniform( skybox_shader.program, "u_orientation" ); ASSERT( skybox_shader.uniform_orientation >= 0 );
		skybox_shader.uniform_cubemap     = glsl_bind_uniform( skybox_shader.program, "u_cubemap" );     ASSERT( skybox_shader.uniform_cubemap >= 0 );

		assert( gl_error() == GL_NO_ERROR );
	}


	skybox_texture = tex_create( );
	assert( gl_error() == GL_NO_ERROR );

	if( skybox_texture )
	{
			glActiveTexture( GL_TEXTURE0 );
			assert( gl_error() == GL_NO_ERROR );

			bool skybox_setup = tex_cube_map_setup( skybox_texture, "./assets/env/sorbin_rt.png", "./assets/env/sorbin_lf.png",
			                                                        "./assets/env/sorbin_up.png", "./assets/env/sorbin_dn.png",
			                                                        "./assets/env/sorbin_ft.png", "./assets/env/sorbin_bk.png" );

			if( !skybox_setup )
			{
				fprintf( stderr, "[ERROR] Failed to setup sky box.\n" );
				exit( EXIT_FAILURE );
			}

			assert( gl_error() == GL_NO_ERROR );
	}
	else
	{
		dump_sdl_error( );
	}

	/* Generate skybox VAO and VBOs */
	{
		glGenVertexArrays( 1, &skybox_vao );  ASSERT( skybox_vao > 0 );
		glBindVertexArray( skybox_vao );

		if( buffer_create( &skybox_vbo, skybox_vertices, sizeof(GLfloat), sizeof(skybox_vertices)/sizeof(skybox_vertices[0]), GL_ARRAY_BUFFER, GL_STATIC_DRAW ) )
		{
			assert( gl_error() == GL_NO_ERROR );
			glEnableVertexAttribArray( skybox_shader.attribute_vertex );
			assert( gl_error() == GL_NO_ERROR );
			glVertexAttribPointer( skybox_shader.attribute_vertex, 3, GL_FLOAT, GL_FALSE, 0, 0 );
			assert( gl_error() == GL_NO_ERROR );
			glDisableVertexAttribArray( skybox_shader.attribute_vertex );
			assert( gl_error() == GL_NO_ERROR );
		}
		else
		{
			dump_sdl_error( );
		}

	}
}

void render( void )
{
	uint32_t now = SDL_GetTicks( );
	delta = frame_delta( now /* milliseconds */ );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );

	camera_update( camera, delta );

    render_skybox();
	render_terrain( );
	render_coordinate_axes( );

	int width; int height;
	SDL_GetWindowSize( window, &width, &height );
	ASSERT( width > 0 && height > 0 );

	glViewport(0, 0, width, height );

	raster_font_shadowed_writef( font2, &VEC2(2, 2 + 8 * 1.5f ), &VEC3(1,1,0), &VEC3_ZERO, 0.92f, "Fractal Terrain Generation Demo" );

	vec3_t forward = camera_forward_vector( camera );
	raster_font_shadowed_writef( font1, &VEC2(2, 2), &VEC3(1,1,1), &VEC3_ZERO, 1.0f, "FPS: %.1f, Direction: (%.1f, %.1f, %.1f)", frame_rate(delta), forward.x, forward.y, forward.z );

	raster_font_shadowed_writef( font1, &VEC2(860, 2), &VEC3(0,1,1), &VEC3_ZERO, 1.0f, "Coded by Joe Marrero" );

	SDL_GL_SwapWindow( window );
	assert( gl_error() == GL_NO_ERROR );
}

void render_terrain( void )
{
	int width; int height;
	SDL_GetWindowSize( window, &width, &height );
	ASSERT( width > 0 && height > 0 );

	glViewport(0, 0, width, height );
	glScissor( 0, 0, width, height );
	assert( gl_error() == GL_NO_ERROR );

	const mat4_t* projection = camera_projection_matrix(camera);
	const mat4_t* model      = camera_model_matrix( camera );


	glUseProgram( terrain_shader.program );
	glBindVertexArray( terrain_vao );
	glEnableVertexAttribArray( skybox_shader.attribute_vertex );
	assert( gl_error() == GL_NO_ERROR );
	#ifndef TERRAIN_VERTEX_HAS_COLOR
	GLfloat color[] = { 0.8f, 0.8f, 0.8f };
	glUniform3fv( terrain_shader.uniform_color, 1, color );
	#endif
	glUniformMatrix4fv( terrain_shader.uniform_projection, 1, GL_FALSE, projection->m );
	assert( gl_error() == GL_NO_ERROR );
	glUniformMatrix4fv( terrain_shader.uniform_model, 1, GL_FALSE, model->m );
	assert( gl_error() == GL_NO_ERROR );
	glUniform1i( terrain_shader.uniform_texture, 0 );
	assert( gl_error() == GL_NO_ERROR );

	const mat3_t normal_matrix = camera_normal_matrix( camera );
	glUniformMatrix3fv( terrain_shader.uniform_normal_matrix, 1, GL_FALSE, normal_matrix.m );
	assert( gl_error() == GL_NO_ERROR );

	glUniform3fv( terrain_shader.uniform_light_direction, 1, (float*) &light_direction );
	assert( gl_error() == GL_NO_ERROR );
	glUniform3fv( terrain_shader.uniform_light_intensity, 1, (float*) &light_intensity );
	assert( gl_error() == GL_NO_ERROR );

	glUniform1i( terrain_shader.uniform_render_grid, 0 );
	assert( gl_error() == GL_NO_ERROR );

	size_t size = fractal_terrain_gen_geometry_size( terrain_gen );

	glBindVertexArray( terrain_vao );
		glBindTexture( GL_TEXTURE_2D, terrain_texture );
		glDrawArrays( tri_strip ? GL_TRIANGLE_STRIP : GL_POINTS, 0, size );
	assert( gl_error() == GL_NO_ERROR );
	glDisableVertexAttribArray( skybox_shader.attribute_vertex );
	glBindVertexArray( 0 );
	assert( gl_error() == GL_NO_ERROR );
}

void render_coordinate_axes( void )
{
	int screen_width; int screen_height;
	SDL_GetWindowSize( window, &screen_width, &screen_height );
	ASSERT( screen_width > 0 && screen_height > 0 );

	const int size = 50;
	glViewport( screen_width - size, 0, size, size );
	glScissor( screen_width - size, 0, size, size );

	const mat4_t* orientation = camera_orientation_matrix( camera );
	mat4_t transform = m3d_translate( &VEC3( 0.0f, 0.0f, -3.0f ) );
	transform = mat4_mult_matrix( &transform, orientation  );
	const mat4_t* projection = camera_projection_matrix( camera );
	mat4_t model_view = mat4_mult_matrix( projection, &transform );

	glDepthMask( GL_FALSE );
	axes_3d_render( axes, model_view.m );
	glDepthMask( GL_TRUE );
}

void render_skybox( void )
{
	glDisable( GL_CULL_FACE );
	int width; int height;
	SDL_GetWindowSize( window, &width, &height );
	ASSERT( width > 0 && height > 0 );

	glViewport(0, 0, width, height );
	glScissor( 0, 0, width, height );
	assert( gl_error() == GL_NO_ERROR );

	glUseProgram( skybox_shader.program );
	assert( gl_error() == GL_NO_ERROR );

	glBindVertexArray( skybox_vao );
	glEnableVertexAttribArray( skybox_shader.attribute_vertex );
	assert( gl_error() == GL_NO_ERROR );
	glUniformMatrix4fv( skybox_shader.uniform_projection, 1, GL_FALSE, (float*) camera_projection_matrix(camera) );
	assert( gl_error() == GL_NO_ERROR );
	glUniformMatrix4fv( skybox_shader.uniform_orientation, 1, GL_FALSE, (float*) camera_orientation_matrix(camera) );
	assert( gl_error() == GL_NO_ERROR );

    glBindTexture( GL_TEXTURE_CUBE_MAP, skybox_texture  );
	assert( gl_error() == GL_NO_ERROR );
	glDepthMask( GL_FALSE );
	assert( gl_error() == GL_NO_ERROR );
	glDrawArrays( GL_TRIANGLES, 0, sizeof(skybox_vertices)/sizeof(skybox_vertices[0]) );
	assert( gl_error() == GL_NO_ERROR );
	glDepthMask( GL_TRUE );
	assert( gl_error() == GL_NO_ERROR );

	glDisableVertexAttribArray( skybox_shader.attribute_vertex );
	glBindVertexArray( 0 );
	assert( gl_error() == GL_NO_ERROR );
	glEnable( GL_CULL_FACE );
}

void dump_sdl_error( void )
{
	const char* sdl_error = SDL_GetError( );

	if( sdl_error && *sdl_error != '\0' )
	{
		fprintf( stderr, "[SDL] %s\n", sdl_error );
	}
}

void set_view_by_mouse( )
{
	int screen_width;
	int screen_height;
	SDL_GetWindowSize( window, &screen_width, &screen_height );


	int middle_x = screen_width / 2; // the middle of the screen in the x direction
	int middle_y = screen_height / 2; // the middle of the screen in the y direction


	// the coordinates of our mouse coordinates
	int mouse_x;
	int mouse_y;
	// This function gets the position of the mouse
	SDL_GetMouseState( &mouse_x, &mouse_y );

	// if the mouse hasn't moved, return without doing
	// anything to our view
	if (mouse_x == middle_x && mouse_y == middle_y ) return;

	// otherwise move the mouse back to the middle of the screen
	SDL_WarpMouseInWindow( window, middle_x, middle_y );

	// get the distance and direction the mouse moved in x (in
	// pixels). We can't use the actual number of pixels in radians,
	// as only six pixels  would cause a full 360 degree rotation.
	// So we use a mousesensitivity variable that can be changed to
	// vary how many radians we want to turn in the x-direction for
	// a given mouse movement distance

	// We have to remember that positive rotation is counter-clockwise.
	// Moving the mouse down is a negative rotation about the x axis
	// Moving the mouse right is a negative rotation about the y axis
	// vector that describes mouseposition - center
	vec2_t mouse_direction = VEC2(
        (mouse_x - middle_x) * mouse_sensitivity,
		(mouse_y - middle_y) * mouse_sensitivity
	);

    if( scaler_abs(mouse_direction.x) > m3d_to_radians(0.05f) || scaler_abs(mouse_direction.y) > m3d_to_radians(0.05f) )
    {
        camera_offset_orientation( camera, mouse_direction.x, mouse_direction.y );
    }
}
