#version 150

//#define DEBUG_LIGHTING
//#define TERRAIN_VERTEX_HAS_COLOR

in vec3 f_vertex;
in vec3 f_normal;
in vec2 f_tex_coord;
in vec4 f_vertex_world;
in vec4 f_color;

uniform mat3 u_normal_matrix;
uniform vec3 u_light_direction;
uniform vec3 u_light_intensity; // Light source intensity
uniform sampler2D u_texture;
uniform int u_render_grid;

out vec4 color;

const float EPSILON      = 0.001f;
const float BORDER_WIDTH = 0.005;
const vec4 GRID_COLOR    = vec4( 1.0, 1.0, 0.0, 1.0 );
const vec4 FOG_COLOR     = vec4( 0.0, 0.0, 0.0, 1.0 );

vec4 fog_linear( vec4 color, vec4 fogColor, float fogNear, float fogFar ) {
	#if 1
	float dist = length(f_vertex_world);
	#else
	float dist = abs(f_vertex_world.z);
	#endif

	float fogFactor = clamp( (fogFar - dist)/(fogFar - fogNear), 0.0, 1.0 );
	return mix( fogColor, color, fogFactor );
}

vec4 fog_exp( vec4 color, vec4 fogColor, float fogDensity ) {
	#if 1
	float dist = length(f_vertex_world);
	#else
	float dist = abs(f_vertex_world.z);
	#endif

	float fogFactor = clamp( 1.0 / exp(dist * fogDensity), 0.0, 1.0 );
	return mix( fogColor, color, fogFactor );
}

void main( ) {
		
	vec4 grid_color = vec4(0.0, 0.0, 0.0, 0.0 );

	if( u_render_grid != 0 )
	{
		grid_color += mix(vec4(0.0, 0.0, 0.0, 0.0), GRID_COLOR, step(fract(f_tex_coord.x), BORDER_WIDTH));
		grid_color += mix(vec4(0.0, 0.0, 0.0, 0.0), GRID_COLOR, step(fract(f_tex_coord.y), BORDER_WIDTH));
		//grid_color += mix(vec4(0.0, 0.0, 0.0, 0.0), GRID_COLOR, smoothstep(fract(f_tex_coord.x - 0.001), fract(f_tex_coord.x + 0.001), 0.005) );
		//grid_color += mix(vec4(0.0, 0.0, 0.0, 0.0), GRID_COLOR, smoothstep(fract(f_tex_coord.y - 0.001), fract(f_tex_coord.y + 0.001), 0.005) );
	}

	// Convert normal and position to eye coords
	vec3 tnorm = normalize( u_normal_matrix * f_normal );
	vec3 light_direction_eye_space = u_normal_matrix * u_light_direction;

	vec3 diffuse_lighting = u_light_intensity * max( dot(light_direction_eye_space, tnorm), 0.0f );
	vec4 light_intensity = vec4( diffuse_lighting, 1.0f );




	

	#if defined(TERRAIN_VERTEX_HAS_COLOR)
	color = f_color;
	#elif defined(DEBUG_LIGHTING)
	color = f_color * light_intensity;
	#else
	color = mix( texture( u_texture, f_tex_coord ) * light_intensity, grid_color, grid_color.a );
	color = fog_exp( color, FOG_COLOR, 0.005 );
	#endif
}
