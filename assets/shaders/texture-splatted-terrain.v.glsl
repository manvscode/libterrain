#version 150

//#define TERRAIN_VERTEX_HAS_COLOR

in vec3 a_vertex;
in vec3 a_normal;
in vec2 a_tex_coord;
#ifdef TERRAIN_VERTEX_HAS_COLOR
in vec3 a_color;
#else
uniform vec3 u_color;
#endif

out vec3 f_vertex;
out vec3 f_normal;
out vec2 f_tex_coord;
out vec4 f_color;
out vec4 f_vertex_world;

uniform mat4 u_projection;
uniform mat4 u_model;
uniform sampler2D u_texture;

void main( ) {
	f_vertex       = a_vertex;
	f_normal       = a_normal;
	f_tex_coord    = a_tex_coord;	
	f_vertex_world = u_model * vec4( a_vertex, 1.0f );
#ifdef TERRAIN_VERTEX_HAS_COLOR
	f_color     = vec4( a_color, 1.0f );
#else
	f_color     = vec4( u_color, 1.0f );
	//f_color     = vec4( 1.0f, 1.0f, 1.0f, 1.0f );
#endif


	gl_Position = u_projection * f_vertex_world;
}
