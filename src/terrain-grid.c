/*
 * Copyright (C) 2013 Joseph A. Marrero.   http://www.manvscode.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <stdlib.h>
#include <string.h>
#ifdef LIBTERRAIN_DEBUG
#include <collections/bench-mark.h>
#endif
#define VECTOR_GROW_AMOUNT(array)      (1)
#include <collections/vector.h>
#include <m3d/mathematics.h>
#include "terrain.h"
#include "terrain-cell.h"

static bool     terrain_grid_setup               ( terrain_grid_t* tg, bool uvs_mapped_to_grid, float scale_uvs );
static bool     terrain_grid_setup_cells         ( terrain_grid_t* tg, bool uvs_mapped_to_grid, float scale_uvs, bool grid_size_flexible );
static uint32_t terrain_grid_index_for_vertex_at ( terrain_grid_t* tg, int32_t x, int32_t y );
static void     terrain_grid_scale_vertex_uv     ( terrain_grid_t* tg, uint32_t x, uint32_t y, bool whole_map, float scale_uvs );


struct terrain_grid {
	const terrain_mesh_t* mesh;
	uint32_t xcells;
	uint32_t ycells;
	float cell_width;
	float cell_length;
	terrain_cell_t** cells; /* array of terrain_cell_t* */
};



terrain_grid_t* terrain_grid_create( const terrain_mesh_t* mesh, uint32_t xcells, uint32_t ycells, bool uvs_mapped_to_grid, float scale_uvs )
{
	bool cells_created = false;
	terrain_grid_t* tg = NULL;

	if( !mesh )
	{
		goto failure;
	}

	tg = (terrain_grid_t*) malloc( sizeof(terrain_grid_t) );

	if( tg )
	{
		tg->mesh        = mesh;
		//tg->xcells      = (xcells & 1) ? xcells : xcells + 1;
		//tg->ycells      = (ycells & 1) ? ycells : ycells + 1;
		tg->xcells      = xcells;
		tg->ycells      = ycells;
		tg->cell_width  = terrain_mesh_width( tg->mesh ) / tg->xcells;
		tg->cell_length = terrain_mesh_length( tg->mesh ) / tg->ycells;

		lc_vector_create( tg->cells, tg->xcells * tg->ycells );

		if( tg->cells )
		{
			cells_created = true;
		}
		else
		{
			cells_created = false;
			goto failure;
		}

		if( !terrain_grid_setup( tg, uvs_mapped_to_grid, scale_uvs ) )
		{
			goto failure;
		}
	}

	return tg;

failure:
	if( tg && cells_created ) lc_vector_destroy( tg->cells );
	if( tg ) free ( tg );
	return NULL;
}


void terrain_grid_destroy( terrain_grid_t* tg )
{
	assert( tg );
	lc_vector_destroy( tg->cells );
	free( tg );
}

bool terrain_grid_setup( terrain_grid_t* tg, bool uvs_mapped_to_grid, float scale_uvs )
{
	assert( tg );
	bool result = false;

	if( !terrain_grid_setup_cells( tg, uvs_mapped_to_grid, scale_uvs, true ) )
	{
		goto done;
	}

	result = true;

done:
	return result;
}

bool terrain_grid_setup_cells( terrain_grid_t* tg, bool uvs_mapped_to_grid, float scale_uvs, bool grid_size_flexible )
{
	assert( tg );
	bool result = false;

	if( terrain_mesh_is_tri_strip(tg->mesh) )
	{
		/* Already tri-stripped meshes are not supported. We need
		 * to have a list of vertices to work with.
		 */
		goto done;
	}

	/* Check if we xcells is a factor of the element width */
	if( terrain_mesh_element_width(tg->mesh) % tg->xcells != 0 )
	{
		if( grid_size_flexible )
		{
			tg->xcells += (terrain_mesh_element_width(tg->mesh) % tg->xcells);
		}
		else
		{
			goto done;
		}
	}

	/* Check if we ycells is a factor of the element length */
	if( terrain_mesh_element_length(tg->mesh) % tg->ycells != 0 )
	{
		if( grid_size_flexible )
		{
			tg->ycells += (terrain_mesh_element_length(tg->mesh) % tg->ycells);
		}
		else
		{
			goto done;
		}
	}

	size_t estimated_size = tg->xcells * tg->ycells;


	if( lc_vector_capacity( tg->cells ) < estimated_size )
	{
		if( !lc_vector_reserve( tg->cells, estimated_size ) )
		{
			goto done;
		}
	}

	uint32_t vertices_per_xcell = (terrain_mesh_element_width(tg->mesh) / tg->xcells);
	uint32_t vertices_per_ycell = (terrain_mesh_element_length(tg->mesh) / tg->ycells);


	/*
	for( uint32_t t = 0; t < terrain_mesh_element_length(tg->mesh); t++ )
	{
		for( uint32_t s = 0; s < terrain_mesh_element_width(tg->mesh); s++ )
		{
			terrain_grid_scale_vertex_uv( tg, s, t, !uvs_mapped_to_grid, scale_uvs );
		}
	}
	*/

	float j = 0.0f;
	float i = 0.0f;


	/* for each map cell... */
	for( uint32_t ycell = 0; ycell < tg->ycells; ycell++ )
	{
		i = 0.0f;
		for( uint32_t xcell = 0; xcell < tg->xcells; xcell++ )
		{
			size_t cell_vertex_count = 2 * ( (vertices_per_xcell + 1) * (vertices_per_ycell + 1) - 1);
			terrain_cell_t* cell = terrain_cell_create( cell_vertex_count );
			lc_vector_push( tg->cells, cell );

			terrain_cell_set_center( cell, i - (terrain_mesh_width(tg->mesh) / 2.0f), j - (terrain_mesh_length(tg->mesh) / 2.0f) );

			uint32_t start_yvertex = vertices_per_ycell * ycell;
			uint32_t start_xvertex = vertices_per_xcell * xcell;

			/* for each vertex in a map cell block... */
			//uint32_t yvertex = j <= 0.0f ? start_yvertex + 1 : start_yvertex;
			uint32_t yvertex = start_yvertex + 1;
			//uint32_t yvertex = start_yvertex;
			uint32_t xvertex;

			for( ; yvertex <= start_yvertex + vertices_per_ycell; yvertex++ )
			{
				if( yvertex >= terrain_mesh_element_length(tg->mesh) )
					break;


				xvertex = start_xvertex;

				//terrain_grid_scale_vertex_uv( tg, xvertex, yvertex, !uvs_mapped_to_grid, scale_uvs );
				terrain_cell_push_index( cell, terrain_grid_index_for_vertex_at(tg, xvertex, yvertex) );

				for( ; xvertex <= start_xvertex + vertices_per_xcell; xvertex++ )
				{
					if( xvertex >= terrain_mesh_element_width(tg->mesh) )
						break;

					//terrain_grid_scale_vertex_uv( tg, xvertex, yvertex, !uvs_mapped_to_grid, scale_uvs );
					terrain_cell_push_index( cell, terrain_grid_index_for_vertex_at(tg, xvertex, yvertex) );

					//terrain_grid_scale_vertex_uv( tg, xvertex, yvertex - 1, !uvs_mapped_to_grid, scale_uvs );
					terrain_cell_push_index( cell, terrain_grid_index_for_vertex_at(tg, xvertex, yvertex - 1) );
				}

				//terrain_grid_scale_vertex_uv( tg, xvertex - 1, yvertex - 1, !uvs_mapped_to_grid, scale_uvs );
				terrain_cell_push_index( cell, terrain_grid_index_for_vertex_at(tg, xvertex - 1, yvertex - 1) );
			}

			i += tg->cell_width;
		}

		j += tg->cell_length;
	}

	result = true;

done:
	return result;
}

uint32_t terrain_grid_index_for_vertex_at( terrain_grid_t* tg, int32_t x, int32_t y )
{
	assert( tg );
	#if 1
	if( x < 0 ) x = 0;
	if( x >= terrain_mesh_element_width(tg->mesh) ) x = terrain_mesh_element_width(tg->mesh) - 1;
	if( y < 0 ) y = 0;
	if( y >= terrain_mesh_element_length(tg->mesh) ) y = terrain_mesh_element_width(tg->mesh) - 1;

	uint32_t index = y * terrain_mesh_element_width(tg->mesh) + x;
	assert( index < terrain_mesh_element_width(tg->mesh) * terrain_mesh_element_length(tg->mesh) );
	return index;
	#else
	uint32_t index = (y % terrain_mesh_element_length(tg->mesh)) * terrain_mesh_element_width(tg->mesh) + (x % terrain_mesh_element_width(tg->mesh));
	assert( index < terrain_mesh_element_width(tg->mesh) * terrain_mesh_element_length(tg->mesh) );
	return index;
	#endif
}

void terrain_grid_scale_vertex_uv( terrain_grid_t* tg, uint32_t x, uint32_t y, bool whole_map, float scale_uvs )
{
	assert( tg );
	terrain_vertex_t* v = terrain_mesh_vertex( tg->mesh, x, y );

	const float u_scale_factor = scale_uvs / (terrain_mesh_element_width(tg->mesh) - 1);
	const float v_scale_factor = scale_uvs / (terrain_mesh_element_length(tg->mesh) - 1);


	/* scale UVs for the entire terrain map... */
	if( whole_map )
	{
		// fix UV's
		v->texture = VEC2(
			/* U */ x * u_scale_factor,
			/* V */ y * v_scale_factor
		);
	}
	else /* scale UVs for just the map cell... */
	{
		/* fix UV's */
		v->texture = VEC2(
			/* U */ x * tg->xcells * u_scale_factor,
			/* V */ y * tg->ycells * v_scale_factor
		);
	}
}

const terrain_mesh_t* terrain_grid_mesh( const terrain_grid_t* tg )
{
	assert( tg );
	return tg->mesh;
}

uint32_t terrain_grid_xcells( const terrain_grid_t* tg )
{
	assert( tg );
	return tg->xcells;
}

uint32_t terrain_grid_ycells( const terrain_grid_t* tg )
{
	assert( tg );
	return tg->ycells;
}

float terrain_grid_cell_width( const terrain_grid_t* tg )
{
	assert( tg );
	return tg->cell_width;
}

float terrain_grid_cell_length( const terrain_grid_t* tg )
{
	assert( tg );
	return tg->cell_length;
}

const terrain_cell_t* terrain_grid_cell( const terrain_grid_t* tg, uint32_t x, uint32_t y )
{
	assert( tg );
	uint32_t cell_index = x + y * tg->xcells;
	assert( cell_index < lc_vector_size(tg->cells) );
	return (const terrain_cell_t*) tg->cells[ cell_index ];
}


