/*
 * Copyright (C) 2013 Joseph A. Marrero.   http://www.manvscode.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#ifdef LIBTERRAIN_DEBUG
#include <collections/bench-mark.h>
#endif
#include <m3d/mathematics.h>
#include "terrain.h"
#include "terrain-cell.h"
#include "terrain-chunk.h"


struct texture_splatted_terrain {
	uint32_t xchunks;
	uint32_t ychunks;
	const terrain_grid_t* grid;
	uint32_t textures[ TERRAIN_MAX_TEXTURES ];
	terrain_chunk_t chunks[];
};


static void texture_splatted_terrain_initialize( texture_splatted_terrain_t* ts );
static void texture_splatted_terrain_setup_uv_coordinates( terrain_cell_t* cell );

texture_splatted_terrain_t* texture_splatted_terrain_create( const terrain_mesh_t* mesh, uint32_t xchunks, uint32_t ychunks )
{
	texture_splatted_terrain_t* ts = NULL;

	if( !mesh )
	{
		goto failure;
	}

	size_t size_of_chunks = xchunks * ychunks * sizeof(terrain_chunk_t);
	ts = (texture_splatted_terrain_t*) malloc( sizeof(texture_splatted_terrain_t) + size_of_chunks );

	if( ts )
	{
		ts->xchunks = xchunks;
		ts->ychunks = ychunks;

		const float chunk_scale = 1.0f / 3;
		ts->grid    = terrain_grid_create( mesh, ts->xchunks * 3, ts->ychunks * 3, true, 1.0f );

		memset( ts->textures, 0, sizeof(uint32_t) * TERRAIN_MAX_TEXTURES );

		if( !ts->grid )
		{
			goto failure;
		}

		texture_splatted_terrain_initialize( ts );
	}

	return ts;

failure:
	if( ts && ts->grid ) terrain_grid_destroy( (terrain_grid_t*) ts->grid );
	if( ts ) free( ts );
	return NULL;
}


void texture_splatted_terrain_destroy( texture_splatted_terrain_t* ts )
{
	assert( ts );
	terrain_grid_destroy( (terrain_grid_t*) ts->grid );
	free( ts );
}

void texture_splatted_terrain_initialize( texture_splatted_terrain_t* ts )
{
	for( uint32_t j = 0; j < ts->ychunks; j++ )
	{
		for( uint32_t i = 0; i < ts->xchunks; i++ )
		{
			terrain_chunk_t* chunk = &ts->chunks[ j * ts->xchunks + i ];
			terrain_chunk_initialize( chunk );

			for( uint8_t m = 0; m < 3; m++ )
			{
				for( uint8_t n = 0; n < 3; n++ )
				{
					const terrain_cell_t* cell = terrain_grid_cell( ts->grid, i * n, j * m );
					chunk->cells[ m * 3 + n ] = cell;


					/* Adjust the UV coordinates */
					texture_splatted_terrain_setup_uv_coordinates( (terrain_cell_t*) cell );
				}
			}
		}
	}
}

void texture_splatted_terrain_setup_uv_coordinates( terrain_cell_t* cell )
{
	//const size_t count      = terrain_cell_vertex_index_count( cell );
	//const uint32_t* indices = terrain_cell_vertex_indices( cell );

	// TODO: Finished this.
}

uint32_t texture_splatted_terrain_xchunks( const texture_splatted_terrain_t* ts )
{
	assert( ts );
	return ts->xchunks;
}

uint32_t texture_splatted_terrain_ychunks( const texture_splatted_terrain_t* ts )
{
	assert( ts );
	return ts->ychunks;
}

const terrain_grid_t* texture_splatted_terrain_grid( const texture_splatted_terrain_t* ts )
{
	assert( ts );
	return ts->grid;
}

terrain_chunk_t* texture_splatted_terrain_chunk( texture_splatted_terrain_t* ts, uint32_t x, uint32_t y )
{
	assert( ts );
	return &ts->chunks[ y * ts->xchunks + x ];
}

uint32_t texture_splatted_terrain_texture( const texture_splatted_terrain_t* ts, uint8_t layer )
{
	assert( ts );
	return ts->textures[ layer ];
}

void texture_splatted_terrain_set_texture( texture_splatted_terrain_t* ts, uint8_t layer, uint32_t texture )
{
	assert( ts );
	ts->textures[ layer ] = texture;
}
