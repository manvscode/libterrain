/*
 * Copyright (C) 2013 Joseph A. Marrero.   http://www.manvscode.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#ifdef LIBTERRAIN_DEBUG
#include <collections/bench-mark.h>
#endif
#include <m3d/mathematics.h>
#include "terrain.h"
#include "terrain-chunk.h"


uint16_t terrain_chunk_alphamap( const terrain_chunk_t* chunk, uint8_t layer )
{
	assert( chunk );
	return chunk->alphamaps[ layer ];
}

void terrain_chunk_set_alphmap( terrain_chunk_t* chunk, uint8_t layer, uint16_t tilebits )
{
	assert( chunk );
	assert( layer >= 1 );
	assert( layer < (TERRAIN_MAX_TEXTURES - 1) );
	chunk->alphamaps[ layer ] = tilebits;
}

