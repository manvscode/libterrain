/*
 * Copyright (C) 2013 Joseph A. Marrero.   http://www.manvscode.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#ifdef LIBTERRAIN_DEBUG
#include <collections/bench-mark.h>
#endif
#include <collections/vector.h>
#include <m3d/mathematics.h>
#include "terrain.h"
#include "terrain-cell.h"


terrain_cell_t* terrain_cell_create( size_t vertex_count )
{
	terrain_cell_t* tc = (terrain_cell_t*) malloc( sizeof(terrain_cell_t) );

	if( tc )
	{
		tc->center = VEC3_ZERO;
		//memset( tc->layers, 0, sizeof(uint32_t) * MAX_TERRAIN_TEXTURES );
		lc_vector_create( tc->indices, vertex_count );

		if( !tc->indices )
		{
			goto failure;
		}
	}

	return tc;

failure:
	if( tc ) free ( tc );
	return NULL;
}


void terrain_cell_destroy( terrain_cell_t* tc )
{
	lc_vector_destroy( tc->indices );
	free( tc );
}

void terrain_cell_push_index( terrain_cell_t* cell, uint32_t index )
{
	assert( cell );
	lc_vector_push( cell->indices, index );
}

const uint32_t* terrain_cell_vertex_indices( const terrain_cell_t* cell )
{
	return cell->indices;
}

size_t terrain_cell_vertex_index_count( const terrain_cell_t* cell )
{
	assert( cell );
	return lc_vector_size( cell->indices );
}

void terrain_cell_set_center( terrain_cell_t* cell, float x, float z )
{
	assert( cell );
	cell->center.x = x;
	cell->center.y = 0.0f;
	cell->center.z = z;
}

const vec3_t* terrain_cell_center( const terrain_cell_t* cell )
{
	assert( cell );
	return &cell->center;
}

