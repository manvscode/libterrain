/*
 * Copyright (C) 2013 Joseph A. Marrero.   http://www.manvscode.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define VECTOR_GROW_AMOUNT(array)      (3)
#include <collections/vector.h>
#include <m3d/mathematics.h>
#include <m3d/geometric-tools.h>
#include <libimageio/imageio.h>
#include "terrain.h"


static void fractal_terrain_gen_random_push    ( fractal_terrain_gen_t* ftg, bool tri_strip, bool post_smooth, float scale_uvs );
static void fractal_terrain_gen_diamond_square ( fractal_terrain_gen_t* ftg, bool tri_strip, bool post_smooth, float scale_uvs );
static void diamond_square( fractal_terrain_gen_t* ftg, uint32_t x, uint32_t y, uint32_t width, uint32_t height, float p );
static vec3_t fractal_terrain_gen_calculate_normal( fractal_terrain_gen_t* ftg, int32_t i, int32_t j );

struct fractal_terrain_gen {
	/* unit width: the smallest width between two vertices along x-axis */
	float unit_width;
 	/* unit length: the smallest length between two vertices along z-axis */
	float unit_length;
	/* element width: the number of possible indices along x-axis */
	uint32_t element_width;
	/* element length: the number of possible indices along z-axis */
	uint32_t element_length;

	uint32_t terrain_width;
	uint32_t terrain_length;
	float    terrain_max_height; // TODO: not used
	float    terrain_min_height; // TODO: not used

	float  push;
	float* height_field;

	terrain_vertex_t* geometry;
};

static inline uint32_t get_index( const fractal_terrain_gen_t* ftg, int32_t x, int32_t z, bool wrap )
{
	if( wrap )
	{
		return (z % ftg->element_length) * (ftg->element_width) + (x % ftg->element_width);
	}
	else
	{
		if( x < 0 ) x = 0;
		if( x >= ftg->terrain_width ) x = ftg->terrain_width - 1;
		if( z < 0 ) z = 0;
		if( z >= ftg->terrain_length ) z = ftg->terrain_length - 1;

		return z * ftg->terrain_width + x;
	}
}

static inline float new_push( float push_factor )
{
	#if 0
	return m3d_guassianf( 0.0f, push_factor );
	#else
	return m3d_uniform_unitf() * push_factor;
	#endif
}


fractal_terrain_gen_t* fractal_terrain_gen_create( uint32_t width, uint32_t length, float min_height, float max_height, float average_push, uint32_t seed )
{
	fractal_terrain_gen_t* ftg = (fractal_terrain_gen_t*) malloc( sizeof(fractal_terrain_gen_t) );

	if( ftg )
	{
		if( width < 1.0f || length < 1.0f )
		{
			ftg->height_field = NULL;
			goto failure;
		}

		ftg->terrain_width      = m3d_next_power_of_2( width );
		ftg->terrain_length     = m3d_next_power_of_2( length );
		ftg->terrain_max_height = max_height;
		ftg->terrain_min_height = min_height;
		ftg->push               = average_push;

		ftg->height_field = (float*) calloc( ftg->terrain_width * ftg->terrain_length, sizeof(float) );
		if( !ftg->height_field ) goto failure;

		memset( ftg->height_field, 0, sizeof(float) * ftg->terrain_width * ftg->terrain_length );

		lc_vector_create( ftg->geometry, ftg->terrain_width * ftg->terrain_length );

		if( !ftg->geometry )
		{
			goto failure;
		}

		srand( seed );
	}

	return ftg;

failure:
	if( ftg )
	{
		if( ftg->height_field ) free( ftg->height_field );
		free( ftg );
	}
	return NULL;
}

void fractal_terrain_gen_destroy( fractal_terrain_gen_t* ftg )
{
	if( ftg )
	{
		lc_vector_destroy( ftg->geometry );
		free( ftg->height_field );
		free( ftg );
	}
}

void fractal_terrain_gen_generate( fractal_terrain_gen_t* ftg, fractal_terrain_algorithm_t alg, float scale_uvs, bool tri_strip )
{
	switch( alg )
	{
		case FTG_RANDOMIZED_PUSH:
			fractal_terrain_gen_random_push( ftg, tri_strip, true, scale_uvs );
			break;
		case FTG_DIAMOND_SQUARE:
			fractal_terrain_gen_diamond_square( ftg, tri_strip, true /*smoothing*/, scale_uvs );
		default:
			break;
	}
}

static inline vec3_t fractal_terrain_vertex( fractal_terrain_gen_t* ftg, int32_t i, int32_t j )
{
	return VEC3( i - 0.5f * ftg->terrain_width, ftg->height_field[ get_index( ftg,  i, j, true ) ], j - 0.5f * ftg->terrain_length );
}

void fractal_terrain_gen_random_push( fractal_terrain_gen_t* ftg, bool tri_strip, bool post_smooth, float scale_uvs )
{
	ftg->unit_width     = 1.0f;
	ftg->unit_length    = 1.0f;
	ftg->element_width  = (uint32_t) ftg->terrain_width;
	ftg->element_length = (uint32_t) ftg->terrain_length;

	lc_vector_clear( ftg->geometry );

	/* pass one: populate terrain heights; Note the += is neccessary with preseeding */
	for( uint32_t j = 0; j < ftg->element_length - 1; j++ )
	{
		for( uint32_t i = 0; i < ftg->element_width - 1; i++ )
		{
			ftg->height_field[ get_index( ftg,  i, j, true ) ] += new_push( ftg->push );
		}
	}

	/* pass two: smooth terrain out. */
	if( post_smooth )
	{
		uint32_t dst_index = 0;
		int8_t smoothing_passes = 15;

		while( smoothing_passes-- > 0 )
		{
			for( uint32_t j = 0; j < ftg->element_length - 1; j++ )
			{
				for( uint32_t i = 0; i < ftg->element_width - 1; i++ )
				{
					dst_index = get_index( ftg,  i, j, true );
					ftg->height_field[ dst_index ] = (ftg->height_field[ dst_index ] +
													  ftg->height_field[ get_index( ftg,  i-1, j, true ) ] +
													  ftg->height_field[ get_index( ftg,  i+1, j, true ) ] +
													  ftg->height_field[ get_index( ftg,  i, j-1, true ) ] +
													  ftg->height_field[ get_index( ftg,  i, j+1, true ) ]
													 ) / 5.0f;
				}
			}
		}
	}

	const size_t geometry_count = tri_strip ? (2 * (ftg->element_width * ftg->element_length - 1)) : (ftg->element_width * ftg->element_length);
	if( !lc_vector_reserve( ftg->geometry, geometry_count ) )
	{
		/* TODO: Error handling... */
	}

	const float u_scale_factor = scale_uvs / ftg->terrain_width;
	const float v_scale_factor = scale_uvs / ftg->terrain_length;

	/* pass three: add extra vertices for degenerate triangles. */
	terrain_vertex_t v1;
	terrain_vertex_t v2;
	terrain_vertex_t v3;
	uint32_t i = 0;
	uint32_t j = tri_strip ? 1 : 0;

	for( ; j < ftg->element_length; j += 1 )
	{
		i = 0;
		v1.position = VEC3( i - 0.5f * ftg->terrain_width, ftg->height_field[ get_index( ftg,  0, j, true ) ], j - 0.5f * ftg->terrain_length );
		v1.normal   = VEC3_YUNIT;
		v1.texture  = VEC2( i * u_scale_factor, j * v_scale_factor );
		#ifdef TERRAIN_VERTEX_HAS_COLOR
		v1.color    = VEC3( 0.0f, 0.5f + 1.0f * ftg->height_field[ get_index( ftg,  0 + 1, j, true ) ], 0.0f );
		#endif
		if( tri_strip ) lc_vector_push( ftg->geometry, v1 );

		for( ; i < ftg->element_width; i += 1 )
		{
			v2.position = VEC3( i - 0.5f * ftg->terrain_width, ftg->height_field[ get_index( ftg,  i, j, true ) ], j - 0.5f * ftg->terrain_length );
			v2.texture  = VEC2( i * u_scale_factor, j * v_scale_factor );
			#ifdef TERRAIN_VERTEX_HAS_COLOR
			v2.color    = VEC3( 0.0f, 0.5f + 1.0f * ftg->height_field[ get_index( ftg,  i , j, true ) ], 0.0f );
			#endif

			v3.position = VEC3( i - 0.5f * ftg->terrain_width, ftg->height_field[ get_index( ftg,  i, j - 1, true ) ], (j - 1) - 0.5f * ftg->terrain_length );
			v3.texture  = VEC2( i * u_scale_factor, (j - 1) * v_scale_factor );
			#ifdef TERRAIN_VERTEX_HAS_COLOR
			v3.color    = VEC3( 0.0f, 0.5f + 1.0f * ftg->height_field[ get_index( ftg,  i, j - 1, true ) ], 0.0f );
			#endif

			vec3_t normal_for_v2 = fractal_terrain_gen_calculate_normal( ftg, i, j );
			vec3_t normal_for_v3 = fractal_terrain_gen_calculate_normal( ftg, i, j - 1 );

			v2.normal = normal_for_v2;
			v3.normal = normal_for_v3;

			lc_vector_push( ftg->geometry, v2 ); /* intentionally not part of tri_strip */
			if( tri_strip ) lc_vector_push( ftg->geometry, v3 );
		}

		if( tri_strip ) lc_vector_push( ftg->geometry, v3 );
	}
}

void fractal_terrain_gen_diamond_square( fractal_terrain_gen_t* ftg, bool tri_strip, bool post_smooth, float scale_uvs )
{
	ftg->unit_width     = 1.0f;
	ftg->unit_length    = 1.0f;
	ftg->element_width  = (uint32_t) ftg->terrain_width;
	ftg->element_length = (uint32_t) ftg->terrain_length;

	lc_vector_clear( ftg->geometry );

	/* pass one: generate heightfield */
	diamond_square( ftg, 0, 0, ftg->terrain_width, ftg->terrain_length, ftg->push );

	/* pass two: smooth terrain out. */
	if( post_smooth )
	{
		uint32_t dst_index = 0;

		for( uint32_t j = 1; j < ftg->element_length - 1; j++ )
		{
			for( uint32_t i = 1; i < ftg->element_width - 1; i++ )
			{
				dst_index = get_index( ftg,  i, j, true );
				#if 0
				ftg->height_field[ dst_index ] = (ftg->height_field[ dst_index ] +
												  ftg->height_field[ get_index( ftg,  i-1, j, true ) ] +
												  ftg->height_field[ get_index( ftg,  i+1, j, true ) ] +
												  ftg->height_field[ get_index( ftg,  i, j-1, true ) ] +
												  ftg->height_field[ get_index( ftg,  i, j+1, true ) ]
												 ) / 5.0f;
				#else
				ftg->height_field[ dst_index ] = (ftg->height_field[ dst_index ] +
												  ftg->height_field[ get_index( ftg,  i-1, j, true ) ] +
												  ftg->height_field[ get_index( ftg,  i, j-1, true ) ]
												 ) / 3.0f;
				#endif
			}
		}
	}

	/* pass three: map the heights to the desired min and max */
	if( true )
	{
		size_t len = ftg->terrain_width * ftg->terrain_length;
		float minh = 0.0f;
		float maxh = 0.0f;

		for( size_t i = 0; i < len; i++ )
		{
			float h = ftg->height_field[ i ];
			minh = m3d_minf( h, minh );
			maxh = m3d_maxf( h, maxh );
		}

		for( size_t i = 0; i < len; i++ )
		{
			float h = ftg->height_field[ i ];
			ftg->height_field[ i ] = (2.0f * h / (maxh - minh)) * ftg->terrain_max_height - ftg->terrain_min_height;
		}
	}

	//#ifdef LIBTERRAIN_DEBUG
	#if 1
	{
		image_t debug_img;
		debug_img.width          = ftg->element_width;
		debug_img.height         = ftg->element_length;
		debug_img.bit_depth      = 8;
		size_t len               = ftg->element_width * ftg->element_length;
		debug_img.pixels         = (uint8_t*) malloc( sizeof(uint8_t) * len );
		if( debug_img.pixels )
		{
			memset( debug_img.pixels, 0, len * sizeof(uint8_t) );

			double min = 0;
			double max = 0;
			for( size_t i = 0; i < len; i++ )
			{
				min = m3d_minf( ftg->height_field[ i ], min );
				max = m3d_maxf( ftg->height_field[ i ], max );

			}
			for( size_t i = 0; i < len; i++ )
			{
				double d = (ftg->height_field[ i ] / (max - min));

				assert( d >= -1.0 && d <= 1.0 );

				debug_img.pixels[ i ] = (uint8_t) ((d * 127) + 128);
			}

			if( !imageio_image_save( &debug_img, "fractal-debug.png", IMAGEIO_PNG ) )
			{
				perror( "ERROR" );
			}
		}
	}
	#endif

	const size_t geometry_count = tri_strip ? (2 * (ftg->element_width * ftg->element_length - 1)) : (ftg->element_width * ftg->element_length);
	if( !lc_vector_reserve( ftg->geometry, geometry_count ) )
	{
		/* TODO: Error handling... */
	}

	const float u_scale_factor = scale_uvs / ftg->terrain_width;
	const float v_scale_factor = scale_uvs / ftg->terrain_length;

	terrain_vertex_t v1;
	terrain_vertex_t v2;
	terrain_vertex_t v3;
	uint32_t i = 0;
	uint32_t j = tri_strip ? 1 : 0;

	/* pass four: add extra vertices for degenerate triangles. */
	for( ; j < ftg->element_length; j += 1 )
	{
		i = 0;
		v1.position = VEC3( i - 0.5f * ftg->terrain_width, ftg->height_field[ get_index( ftg,  0, j, true ) ], j - 0.5f * ftg->terrain_length );
		v1.texture  = VEC2( i * u_scale_factor, j * v_scale_factor );
		v1.normal   = VEC3_YUNIT;
		#ifdef TERRAIN_VERTEX_HAS_COLOR
		v1.color    = VEC3( 0.0f, 0.5f + 1.0f * ftg->height_field[ get_index( ftg,  0 + 1, j, true ) ], 0.0f );
		#endif

		if( tri_strip) lc_vector_push( ftg->geometry, v1 );

		for( ; i < ftg->element_width; i += 1 )
		{
			v2.position = VEC3( i - 0.5f * ftg->terrain_width, ftg->height_field[ get_index( ftg,  i, j, true ) ], j - 0.5f * ftg->terrain_length );
			v2.texture  = VEC2( i * u_scale_factor, j * v_scale_factor );
			#ifdef TERRAIN_VERTEX_HAS_COLOR
			v2.color    = VEC3( 0.0f, 0.5f + 1.0f * ftg->height_field[ get_index( ftg,  i , j, true ) ], 0.0f );
			#endif

			v3.position = VEC3( i - 0.5f * ftg->terrain_width, ftg->height_field[ get_index( ftg,  i, j - 1, true ) ], (j - 1) - 0.5f * ftg->terrain_length );
			v3.texture  = VEC2( i * u_scale_factor, (j - 1) * v_scale_factor );
			#ifdef TERRAIN_VERTEX_HAS_COLOR
			v3.color    = VEC3( 0.0f, 0.5f + 1.0f * ftg->height_field[ get_index( ftg,  i, j - 1, true ) ], 0.0f );
			#endif

			vec3_t normal_for_v2 = fractal_terrain_gen_calculate_normal( ftg, i, j );
			vec3_t normal_for_v3 = fractal_terrain_gen_calculate_normal( ftg, i, j - 1 );

			v2.normal = normal_for_v2;
			v3.normal = normal_for_v3;

			lc_vector_push( ftg->geometry, v2 ); /* intentionally not part of tri_strip */
			if( tri_strip) lc_vector_push( ftg->geometry, v3 );
		}
		if( tri_strip) lc_vector_push( ftg->geometry, v3 );
	}
}

uint32_t fractal_terrain_gen_element_width( const fractal_terrain_gen_t* ftg )
{
	return ftg->element_width;
}

uint32_t fractal_terrain_gen_element_length( const fractal_terrain_gen_t* ftg )
{
	return ftg->element_length;
}

float fractal_terrain_gen_unit_width( const fractal_terrain_gen_t* ftg )
{
	return ftg->unit_width;
}

float fractal_terrain_gen_unit_length( const fractal_terrain_gen_t* ftg )
{
	return ftg->unit_length;
}

float fractal_terrain_gen_width( const fractal_terrain_gen_t* ftg )
{
	return ftg->terrain_width;
}

float fractal_terrain_gen_length( const fractal_terrain_gen_t* ftg )
{
	return ftg->terrain_length;
}

float fractal_terrain_gen_max_height( const fractal_terrain_gen_t* ftg )
{
	return ftg->terrain_max_height;
}

float fractal_terrain_gen_min_height( const fractal_terrain_gen_t* ftg )
{
	return ftg->terrain_min_height;
}

void fractal_terrain_gen_seed_height( fractal_terrain_gen_t* ftg, uint32_t x, uint32_t z, float height )
{
	ftg->height_field[ get_index(ftg, x, z, true) ] = height;
}

terrain_vertex_t* fractal_terrain_gen_geometry( const fractal_terrain_gen_t* ftg )
{
	return ftg->geometry;
}

size_t fractal_terrain_gen_geometry_size( const fractal_terrain_gen_t* ftg )
{
	return lc_vector_size( ftg->geometry );
}

static inline void sample_square( fractal_terrain_gen_t* ftg, uint32_t x, uint32_t y, int32_t half_width, int32_t half_height, float p )
{
	if( half_height <= 0 ) return;
	if( half_width <= 0 ) return;

	p = new_push( p );
   /*    a--+--b
	*    |  |  |  Square step
	*    +--e--+
	*    |  |  |
	*    d--+--c
	*/
	float a = ftg->height_field[ get_index(ftg, x - half_width, y - half_height, true) ];
	float b = ftg->height_field[ get_index(ftg, x + half_width, y - half_height, true) ];
	float c = ftg->height_field[ get_index(ftg, x + half_width, y + half_height, true) ];
	float d = ftg->height_field[ get_index(ftg, x - half_width, y + half_height, true) ];

	float f = (a + b) / 2.0f;
	float g = (b + c) / 2.0f;
	float h = (c + d) / 2.0f;
	float i = (d + a) / 2.0f;

	ftg->height_field[ get_index(ftg, x, y - half_height, true) ]   = f + p; // top edge
	ftg->height_field[ get_index(ftg, x + half_width, y, true ) ]   = g + p; // right edge
	ftg->height_field[ get_index(ftg, x, y + half_height, true ) ]  = h + p; // bottom edge
	ftg->height_field[ get_index(ftg, x - half_width, y, true ) ]   = i + p; // left edge

	float e = (a + b + c + d) / 4.0f;

	ftg->height_field[ get_index(ftg, x, y, true) ] = e + p;
}

static inline void sample_diamond( fractal_terrain_gen_t* ftg, uint32_t x, uint32_t y, int32_t half_width, int32_t half_height, float p )
{
	if( half_height <= 0 ) return;
	if( half_width <= 0 ) return;

	p = new_push( p );

   /*    +--f--+
	*    |  |  |  Diamond step
	*    i--j--g
	*    |  |  |
	*    +--h--+
	*/
	float a = ftg->height_field[ get_index(ftg, x - half_width, y - half_height, true) ];
	float b = ftg->height_field[ get_index(ftg, x + half_width, y - half_height, true) ];
	float c = ftg->height_field[ get_index(ftg, x + half_width, y + half_height, true) ];
	float d = ftg->height_field[ get_index(ftg, x - half_width, y + half_height, true) ];

	float f = (a + b) / 2.0f;
	float g = (b + c) / 2.0f;
	float h = (c + d) / 2.0f;
	float i = (d + a) / 2.0f;

	float j = (f + g + h + i) / 4.0f;

	ftg->height_field[ get_index(ftg, x, y, true) ] = j + p;
}

void diamond_square( fractal_terrain_gen_t* ftg, uint32_t x, uint32_t y, uint32_t width, uint32_t height, float p )
{
	/* preseed the corner heights */
	#if 1
	ftg->height_field[ get_index( ftg, 0, 0, true) ]          = new_push( p );
	ftg->height_field[ get_index( ftg, width, 0, true) ]      = new_push( p );
	ftg->height_field[ get_index( ftg, width, height, true) ] = new_push( p );
	ftg->height_field[ get_index( ftg, 0, height, true) ]     = new_push( p );
	#else
	ftg->height_field[ get_index( ftg, 0, 0, true) ]          = uniform_unitf() * p * (ftg->terrain_max_height - ftg->terrain_min_height);
	ftg->height_field[ get_index( ftg, width, 0, true) ]      = uniform_unitf() * p * (ftg->terrain_max_height - ftg->terrain_min_height);
	ftg->height_field[ get_index( ftg, width, height, true) ] = uniform_unitf() * p * (ftg->terrain_max_height - ftg->terrain_min_height);
	ftg->height_field[ get_index( ftg, 0, height, true) ]     = uniform_unitf() * p * (ftg->terrain_max_height - ftg->terrain_min_height);
	#endif

	uint32_t xstride = width << 1;
	uint32_t ystride = height << 1;

	while( xstride > 1 || ystride > 1 )
	{
		uint32_t half_xstride = xstride >> 1;
		uint32_t half_ystride = ystride >> 1;

		for( int y = 0; y < height; y += ystride )
		{
			for( int x = 0; x < width; x += xstride )
			{
				sample_square( ftg, x + half_xstride, y + half_ystride, half_xstride, half_ystride, p );
			}
		}

		for( int y = 0; y < height + half_ystride; y += ystride )
		{
			for( int x = 0; x < width + half_xstride; x += xstride )
			{
				sample_diamond( ftg, x + 0           , y + 0, half_xstride, half_ystride, p );
				sample_diamond( ftg, x + half_xstride, y + 0           , half_xstride, half_ystride, p );
			}
		}

		xstride >>= 1;
		ystride >>= 1;
		p /= 2.0f;
	}
}

vec3_t fractal_terrain_gen_calculate_normal( fractal_terrain_gen_t* ftg, int32_t i, int32_t j )
{
	/*
	 * Every vertex is generally shared among 6 triangles.  We calculate the
	 * normal of each triangle and average them together to calculate the
	 * normal at vertex 0.
  	 *
	 *                             1--2--*
	 *  The numbers are the        |\ |\ |
	 *  vertices that are members  | \| \|
	 *  of the 6 triangles shared  6--0--3
	 *  by vertex 0.               |\ |\ |
	 *                             | \| \|
	 *                             *--5--4
	 */

	vec3_t point0 = fractal_terrain_vertex( ftg,     i,     j );
	vec3_t point1 = fractal_terrain_vertex( ftg, i - 1, j - 1 );
	vec3_t point2 = fractal_terrain_vertex( ftg,     i, j - 1 );
	vec3_t point3 = fractal_terrain_vertex( ftg, i + 1,     j );
	vec3_t point4 = fractal_terrain_vertex( ftg, i + 1, j + 1 );
	vec3_t point5 = fractal_terrain_vertex( ftg,     i, j + 1 );
	vec3_t point6 = fractal_terrain_vertex( ftg, i - 1,     j );

	/* top-left triangle first going CW, triangles using CCW winding */
	const size_t MAX_POINTS = 6 * 3;
	const vec3_t* points[ ] = {
		&point0, &point1, &point6,
		&point0, &point2, &point1,
		&point0, &point3, &point2,
		&point0, &point4, &point3,
		&point0, &point5, &point4,
		&point0, &point6, &point5
	};

	return m3d_normal_from_triangles( points, MAX_POINTS );
}
