/*
 * Copyright (C) 2013 Joseph A. Marrero.   http://www.manvscode.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <assert.h>
#include <stdio.h>
#include <m3d/mathematics.h>
#include <m3d/geometric-tools.h>
#include <libimageio/imageio.h>
#include "terrain.h"



/*
 * The total possible alpha maps can be calculated using combinatorics
 * as:
 *
 *      9C9 + 9C9 + 9C7 + ... + 9C2 + 9C1 + 9C0 = 512
 *
 * There are 512 possible transitions and only 511 of these contain
 * some alpha. Most of these alphamaps may not even be used. At
 * 64x64 @ GL_ALPHA8, an alpha map would require 4096 bytes. For
 * all 512 alpha maps, we would need 512 * 4096 bytes = 2097152
 * bytes = 2 megs
 *
 * This shows how efficient it is on graphics hardware.
 *
 * Now, you want to have N number of textures on your terrain, then
 * you will need N - 1 splats (not counting the base texture). Thus,
 * the total texture memory requirements are (N - 1) * (2 megs).
 *
 * So, if we want 6 textures, then we need 10 megs
 */
static const uint32_t MAX_POSSIBLE_ALPHMAPS = 512;

/*
 *	Alphamap manager for the splat terrain texturing
 *  technique. We map a bitmask to a particular tile
 *  arrangement. This is how I know that a set of tiles
 *  are suppose to have a specific texture.
 */
struct terrain_alphamap {
	uint16_t width;
	uint16_t height;
	uint8_t channels;
	uint16_t alphamap_size;
	size_t all_alphamap_size;
	uint8_t alphamaps[];
};

static bool terrain_alphamap_build_all ( terrain_alphamap_t* am, bool noise, float alpha_bias );
static void terrain_alphamap_build     ( terrain_alphamap_t* am, uint16_t tilebits, bool noise, float alpha_bias );

terrain_alphamap_t* terrain_alphamap_create_default( void )
{
	const size_t channels  = 1;
	const uint16_t width   = TERRAIN_ALPHA_MAP_SIZE;
	const uint16_t height  = TERRAIN_ALPHA_MAP_SIZE;
	const bool noise       = true;
	const float alpha_bias = 1.0f;
	return terrain_alphamap_create( width, height, channels, noise, alpha_bias );
}

terrain_alphamap_t* terrain_alphamap_create( uint16_t width, uint16_t height, uint8_t channels, bool noise, float alpha_bias )
{
	const size_t alphamap_size = width * height * channels;
	const size_t all_alphamap_size = alphamap_size * MAX_POSSIBLE_ALPHMAPS;

	terrain_alphamap_t* am = malloc( sizeof(terrain_alphamap_t) + all_alphamap_size );

	if( am )
	{
		am->width             = width;
		am->height            = height;
		am->channels          = channels;
		am->alphamap_size     = alphamap_size;
		am->all_alphamap_size = all_alphamap_size;

		if( !terrain_alphamap_build_all( am, noise, alpha_bias ) )
		{
			free( am );
			am = NULL;
		}

		#ifdef LIBTERRAIN_DEBUG
		if( am )
		{
			printf( "Created Alphamap %u textures of %ux%u with %u channel\n", MAX_POSSIBLE_ALPHMAPS, am->width, am->height, am->channels );
		}
		#endif
	}

	return am;
}

terrain_alphamap_t* terrain_alphamap_from_file( const char* filename )
{
	FILE* file = fopen( filename, "rb" );
	terrain_alphamap_t* am = NULL;

	if( file )
	{
		terrain_alphamap_t header;
		fread( &header, sizeof(terrain_alphamap_t), 1, file );
		am = malloc( sizeof(terrain_alphamap_t) + header.all_alphamap_size );

		if( am )
		{
			*am = header;
			fread( am->alphamaps, am->all_alphamap_size, 1, file );
		}

		fclose( file );
	}

	return am;
}

void terrain_alphamap_destroy( terrain_alphamap_t* am )
{
	free( am );
}

uint16_t terrain_alphamap_width( const terrain_alphamap_t* am )
{
	return am ? am->width : 0;
}

uint16_t terrain_alphamap_height( const terrain_alphamap_t* am )
{
	return am ? am->height : 0;
}

static inline uint8_t* terrain_alphamap_bitmap_for( terrain_alphamap_t* am, uint16_t tilebits )
{
	return &am->alphamaps[ tilebits * am->alphamap_size ];
}

const void* terrain_alphamap_bitmap( const terrain_alphamap_t* am, uint16_t tilebits )
{
	uint8_t* bitmap = NULL;
	if( am )
	{
		bitmap = terrain_alphamap_bitmap_for( (terrain_alphamap_t*) am, tilebits );
	}

	return bitmap;
}

static inline float terrain_alphamap_distance_squared( float x1, float y1, float x2, float y2 )
{ return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2); }

static inline float terrain_alphamap_weight( float x1, float y1, float x2, float y2 )
{
	// This function depends on the upper and lower bounds you
	// define for element space. I chose to have my bounds set
	// between -1.0 to 1.0 along both dimensions to optimizie
	// the division required by the weight function. IF YOU
	// CHANGE YOUR BOUNDS, THIS WEIGHT FUNCTION MAY NOT WORK!!!
	//
	// If you divide by a value * value where value is less then
	// your bound, you essentially pull back fading...

	const float radius_squared = 0.75f * 0.75f;
	float w = 1 - terrain_alphamap_distance_squared(x1, y1, x2, y2) / radius_squared;
	return w > 0 ? w : 0;
}

static inline float terrain_alphamap_noise( int32_t x )
{
	x = (x<<13) ^ x;
	return ( 1.0f - ( (x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0f);
}

bool terrain_alphamap_build_all( terrain_alphamap_t* am, bool noise, float alpha_bias )
{
	if( !am ) return false;

	for( uint16_t i = 0; i < MAX_POSSIBLE_ALPHMAPS; i++ )
	{
		terrain_alphamap_build( am, i, noise, alpha_bias );
	}

	return true;
}

void terrain_alphamap_build( terrain_alphamap_t* am, uint16_t tilebits, bool noise, float alpha_bias )
{
	uint8_t* alphamap = terrain_alphamap_bitmap_for( am, tilebits );
	/*
	 * By convention, we assume...
	 *     ___________
	 *    | 0 | 1 | 2 |
	 *    |___|___|___|
	 *    | 3 | 4 | 5 |
	 *    |___|___|___|
	 *    | 6 | 7 | 8 |
	 *    |___|___|___|
	 */
	const uint16_t TILE_BIT_MASKS[] = {
		1 << 0,
		1 << 1,
		1 << 2,
		1 << 3,
		1 << 4,
		1 << 5,
		1 << 6,
		1 << 7,
		1 << 8
	};

	for( uint16_t texelY = 0; texelY < am->height; texelY++ )
	{
		for( uint16_t texelX = 0; texelX < am->width; texelX++ )
		{
			float totalContribution = 0.0f;
			float contribution = 0.0f;

			for( uint8_t y = 0; y < 3; y++ )
			{
				for( uint8_t x = 0; x < 3; x++ )
				{
					float texelXInElementSpace = ((float) 2.0f * texelX / (float) am->width) - 1.0f;
					float texelYInElementSpace = ((float) 2.0f * texelY / (float) am->height) - 1.0f;

					assert( texelXInElementSpace >= -1.0f && texelXInElementSpace <= 1.0f );
					assert( texelYInElementSpace >= -1.0f && texelYInElementSpace <= 1.0f );


					float w = terrain_alphamap_weight( x - 1.0f, y - 1.0f, texelXInElementSpace, texelYInElementSpace );
					float n = 1.0f;

					if( noise )
					{
						n = fabsf( terrain_alphamap_noise( rand( ) ) );
					}

					if( TILE_BIT_MASKS[ y * 3 + x ] & tilebits )
					{
						contribution += w * n;
					}

					totalContribution += w * n;
				}
			}

			const float alpha = m3d_clampf( (contribution / totalContribution) * alpha_bias, 0.0f, 1.0f );
			const uint8_t fragment = (uint8_t) 255 * alpha;
			const size_t pixel = texelY * am->width + texelX;


			for( uint16_t c = 0; c < am->channels; c++ )
			{
				alphamap[ pixel + c ] = fragment;
			}
		}
	}

	#if 0
	{
		image_t debug_img;
		debug_img.width          = am->width;
		debug_img.height         = am->height;
		debug_img.bits_per_pixel = am->channels * 8;
		debug_img.pixels         = (uint8_t*) malloc( sizeof(uint8_t) * am->alphamap_size );
		memcpy( debug_img.pixels, alphamap, sizeof(uint8_t) * am->alphamap_size );

		char filename[ 128 ];
		snprintf( filename, sizeof(filename), "alphamap-debug-%03u.png", tilebits );

		if( !imageio_image_save( &debug_img, filename, IMAGEIO_PNG ) )
		{
			perror( "ERROR" );
		}
	}
	#endif
}

bool terrain_alphamap_save_file( const terrain_alphamap_t* am, const char* filename )
{
	FILE* file = fopen( filename, "wb+" );

	if( file )
	{
		fwrite( am, sizeof(terrain_alphamap_t), 1, file );
		fwrite( am->alphamaps, am->all_alphamap_size, 1, file );

		fclose( file );
		return true;
	}

	return false;
}

