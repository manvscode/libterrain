/*
 * Copyright (C) 2013 Joseph A. Marrero.   http://www.manvscode.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifdef LIBTERRAIN_DEBUG
#include <libcollections/bench-mark.h>
#endif
#define VECTOR_GROW_AMOUNT(array)      (3)
#include <collections/vector.h>
#include <m3d/mathematics.h>
#include <m3d/geometric-tools.h>
#include "terrain.h"

#ifdef TERRAIN_VERTEX_HAS_COLOR
static vec3_t terrain_color_ramp       ( const bitmap_terrain_gen_t* tg, float height );
#endif
static size_t bitmap_terrain_index( int32_t i, int32_t j, uint8_t bytes_per_pixel, uint32_t width, uint32_t height, bool wrap );
static vec3_t bitmap_terrain_vertex( bitmap_terrain_gen_t* tg, int32_t i, int32_t j, uint8_t bytes_per_pixel, uint32_t width, uint32_t height, const void* bitmap );
static void   bitmap_terrain_gen_setup ( bitmap_terrain_gen_t* tg, uint32_t width, uint32_t height, uint8_t bytes_per_pixel, const void *bitmap, float scale_uvs, bool tri_strip );

struct bitmap_terrain_gen {
	/* unit width: the smallest width between two vertices along x-axis */
	float unit_width;
 	/* unit length: the smallest length between two vertices along z-axis */
	float unit_length;
	/* element width: the number of possible indices along x-axis */
	uint32_t element_width;
	/* element length: the number of possible indices along z-axis */
	uint32_t element_length;

	float terrain_width;
	float terrain_length;
	float terrain_max_height;
	float terrain_min_height;

	terrain_vertex_t* geometry;
};



bitmap_terrain_gen_t* bitmap_terrain_gen_create( float terrain_width, float terrain_length, float terrain_min_height, float terrain_max_height )
{
	bitmap_terrain_gen_t* tg = malloc( sizeof(bitmap_terrain_gen_t) );

	if( tg )
	{
		tg->unit_width         = 0.0f;
		tg->unit_length        = 0.0f;
		tg->terrain_width      = terrain_width;
		tg->terrain_length     = terrain_length;
		tg->terrain_max_height = terrain_max_height;
		tg->terrain_min_height = terrain_min_height;
		tg->element_width      = 0;
		tg->element_length     = 0;

		size_t estimated_mesh_size = 1;
		#ifdef LIBTERRAIN_DEBUG
		printf( "[Bitmap Terrain] Estimated mesh size = %zd\n", estimated_mesh_size );
		#endif

		lc_vector_create( tg->geometry, estimated_mesh_size );

		if( !tg->geometry )
		{
			goto failure;
		}
	}

	return tg;

failure:
	if( tg ) free( tg );
	return NULL;
}

void bitmap_terrain_gen_destroy( bitmap_terrain_gen_t* tg )
{
	if( tg )
	{
		lc_vector_destroy( tg->geometry );
		free( tg );
	}
}

void bitmap_terrain_gen_generate( bitmap_terrain_gen_t* tg, uint32_t width, uint32_t height, uint8_t bytes_per_pixel, const void *bitmap, float scale_uvs, bool tri_strip )
{
	bitmap_terrain_gen_setup( tg, width, height, bytes_per_pixel, bitmap, scale_uvs, tri_strip );
}

size_t bitmap_terrain_index( int32_t i, int32_t j, uint8_t bytes_per_pixel, uint32_t width, uint32_t height, bool wrap )
{
	if( wrap )
	{
		return (j % height) * width * bytes_per_pixel + (i % width) * bytes_per_pixel;
	}
	else
	{
		if( j < 0 ) j = 0;
		if( j >= height ) j = height - 1;
		if( i < 0 ) i = 0;
		if( i >= width ) i = width - 1;

		return j * width * bytes_per_pixel + i * bytes_per_pixel;
	}
}

vec3_t bitmap_terrain_vertex( bitmap_terrain_gen_t* tg, int32_t i, int32_t j, uint8_t bytes_per_pixel, uint32_t width, uint32_t height, const void* bitmap )
{
	const scaler_t WHITE_VECTOR_MAGNITUDE = scaler_sqrt( 255*255 + 255*255 + 255*255 );
	const float half_terrain_width  = tg->terrain_width / 2.0f;
	const float half_terrain_length = tg->terrain_length / 2.0f;
	const float height_length       = tg->terrain_max_height - tg->terrain_min_height;
	const uint8_t* pixels = bitmap;

	#if 1
	const bool wrap = false;
	size_t position = bitmap_terrain_index( i, j, bytes_per_pixel, width, height, wrap );
	vec3_t color = VEC3( pixels[ position + 0 ], pixels[ position + 1 ], pixels[ position + 2 ] );
	scaler_t calculated_height = ( vec3_magnitude(&color) / WHITE_VECTOR_MAGNITUDE ) * height_length + tg->terrain_min_height;
	return VEC3( tg->unit_width * i - half_terrain_width, calculated_height, tg->unit_length * j - half_terrain_length );
	#else
	const bool wrap = true;
	/* We are on an edge so let's add in contributions from the opposite edge */
	const size_t center = bitmap_terrain_index( i, j, bytes_per_pixel, width, height, wrap );
	const size_t left   = bitmap_terrain_index( i - 1, j, bytes_per_pixel, width, height, wrap );
	const size_t right  = bitmap_terrain_index( i + 1, j, bytes_per_pixel, width, height, wrap );
	const size_t bottom = bitmap_terrain_index( i, j - 1, bytes_per_pixel, width, height, wrap );
	const size_t top    = bitmap_terrain_index( i, j + 1, bytes_per_pixel, width, height, wrap );

	const scaler_t mainWeight  = 0.6f;
	const scaler_t otherWeight = (1.0f - mainWeight) / 4 /* four neighbors */;

	vec3_t centerColor = vec3_multiply( &VEC3( pixels[ center + 0 ], pixels[ center + 1 ], pixels[ center + 2 ] ), mainWeight );
	vec3_t leftColor   = vec3_multiply( &VEC3( pixels[ left + 0 ], pixels[ left + 1 ], pixels[ left + 2 ] ), otherWeight );
	vec3_t rightColor  = vec3_multiply( &VEC3( pixels[ right + 0 ], pixels[ right + 1 ], pixels[ right + 2 ] ), otherWeight );
	vec3_t bottomColor = vec3_multiply( &VEC3( pixels[ bottom + 0 ], pixels[ bottom + 1 ], pixels[ bottom + 2 ] ), otherWeight );
	vec3_t topColor    = vec3_multiply( &VEC3( pixels[ top + 0 ], pixels[ top + 1 ], pixels[ top + 2 ] ), otherWeight );

	vec3_t finalColor = VEC3_ZERO;
	finalColor = vec3_add( &finalColor, &centerColor );
	finalColor = vec3_add( &finalColor, &leftColor );
	finalColor = vec3_add( &finalColor, &rightColor );
	finalColor = vec3_add( &finalColor, &bottomColor );
	finalColor = vec3_add( &finalColor, &topColor );

	scaler_t calculated_height = ( vec3_magnitude(&finalColor) / WHITE_VECTOR_MAGNITUDE ) * height_length + tg->terrain_min_height;
	return VEC3( tg->unit_width * i - half_terrain_width, calculated_height, tg->unit_length * j - half_terrain_length );
	#endif
}


vec3_t bitmap_terrain_gen_calculate_normal( bitmap_terrain_gen_t* tg, int32_t i, int32_t j, uint8_t bytes_per_pixel, uint32_t width, uint32_t height, const void* bitmap )
{
	/*
	 * Every vertex is generally shared among 6 triangles.  We calculate the
	 * normal of each triangle and average them together to calculate the
	 * normal at vertex 0.
  	 *
	 *                             1--2--*
	 *  The numbers are the        |\ |\ |
	 *  vertices that are members  | \| \|
	 *  of the 6 triangles shared  6--0--3
	 *  by vertex 0.               |\ |\ |
	 *                             | \| \|
	 *                             *--5--4
	 */

	vec3_t point0 = bitmap_terrain_vertex( tg,     i,     j, bytes_per_pixel, width, height, bitmap );
	vec3_t point1 = bitmap_terrain_vertex( tg, i - 1, j - 1, bytes_per_pixel, width, height, bitmap );
	vec3_t point2 = bitmap_terrain_vertex( tg,     i, j - 1, bytes_per_pixel, width, height, bitmap );
	vec3_t point3 = bitmap_terrain_vertex( tg, i + 1,     j, bytes_per_pixel, width, height, bitmap );
	vec3_t point4 = bitmap_terrain_vertex( tg, i + 1, j + 1, bytes_per_pixel, width, height, bitmap );
	vec3_t point5 = bitmap_terrain_vertex( tg,     i, j + 1, bytes_per_pixel, width, height, bitmap );
	vec3_t point6 = bitmap_terrain_vertex( tg, i - 1,     j, bytes_per_pixel, width, height, bitmap );

	/* top-left triangle first going CW, triangles using CCW winding */
	const size_t MAX_POINTS = 6 * 3;
	const vec3_t* points[ ] = {
		&point0, &point1, &point6,
		&point0, &point2, &point1,
		&point0, &point3, &point2,
		&point0, &point4, &point3,
		&point0, &point5, &point4,
		&point0, &point6, &point5
	};

	return m3d_normal_from_triangles( points, MAX_POINTS );
}

void bitmap_terrain_gen_setup( bitmap_terrain_gen_t* tg, uint32_t width, uint32_t height, uint8_t bytes_per_pixel, const void *bitmap, float scale_uvs, bool tri_strip )
{

	#ifdef LIBTERRAIN_DEBUG
	printf( "[Bitmap Terrain] Loaded Height Map, %dx%d@%dbpp\n", width, height, bytes_per_pixel << 3 );
	lc_bench_mark_t bm = bench_mark_create( "Terrain Generation, points");
	bench_mark_start( bm );
	#endif
	assert( tg->terrain_width >= width );
	assert( tg->terrain_length >= height );

	lc_vector_clear( tg->geometry );

	const size_t geometry_count = tri_strip ? (2 * (width + 1) * (height)) : (width * height);
	if( !lc_vector_reserve( tg->geometry, geometry_count ) )
	{
		/* TODO: Error handling... */
	}


	tg->unit_width  = tg->terrain_width / width;
	tg->unit_length = tg->terrain_length / height;

	// UV Scaling... Do you want to fit a texture over the entire terrain mesh or over a grid?
	const float u_scale_factor = scale_uvs / (width - 1);
	const float v_scale_factor = scale_uvs / (height - 1);

	tg->element_width  = width;
	tg->element_length = height;


	size_t j = 0;
	size_t i = 0;

	for( ; j < height; j++ )
	{
		i = 0;
		terrain_vertex_t v1;
		terrain_vertex_t v2;
		terrain_vertex_t v3;

		// vert 0
		v1.position = bitmap_terrain_vertex( tg, i, j, bytes_per_pixel, width, height, bitmap );
		v1.texture  = VEC2( i * u_scale_factor, j * v_scale_factor );
		v1.normal   = VEC3(0,1,0);
		#ifdef TERRAIN_VERTEX_HAS_COLOR
		v1.color    = terrain_color_ramp( tg, calculated_height );
		#endif

		if( tri_strip ) lc_vector_push( tg->geometry, v1 );

		for( ; i < width; i++ )
		{
			// vert 1
			v2.position = bitmap_terrain_vertex( tg, i, j, bytes_per_pixel, width, height, bitmap );
			v2.texture  = VEC2( i * u_scale_factor, j * v_scale_factor );
			#ifdef TERRAIN_VERTEX_HAS_COLOR
			v2.color    = terrain_color_ramp( tg, calculated_height );
			#endif

			// vert 2
			v3.position = bitmap_terrain_vertex( tg, i, j - 1, bytes_per_pixel, width, height, bitmap );
			v3.texture  = VEC2( i * u_scale_factor, (j - 1) * v_scale_factor );
			#ifdef TERRAIN_VERTEX_HAS_COLOR
			v3.color    = terrain_color_ramp( tg, calculated_height );
			#endif

			vec3_t normal_for_v2 = bitmap_terrain_gen_calculate_normal( tg, i, j, bytes_per_pixel, width, height, bitmap );
			vec3_t normal_for_v3 = bitmap_terrain_gen_calculate_normal( tg, i, j - 1, bytes_per_pixel, width, height, bitmap );

			v2.normal = normal_for_v2;
			v3.normal = normal_for_v3;
			/*
			vec3_t normal = vec3_cross_product(
				&VEC3( v2.position.x - v1.position.x, v2.position.y - v1.position.y, v2.position.z - v1.position.z ),
				&VEC3( v3.position.x - v1.position.x, v3.position.y - v1.position.y, v3.position.z - v1.position.z )
			);
			vec3_normalize( &normal );

			if( vec3_magnitude(&normal) > SCALAR_EPSILON )
			{
				v2.normal = normal;
				v3.normal = normal;
			}
			else
			{
				v2.normal = VEC3_YUNIT;
				v3.normal = VEC3_YUNIT;
			}
			*/

			#ifdef LIBTERRAIN_DEBUG
			#ifdef TERRAIN_VERTEX_HAS_COLOR
			printf( "[Bitmap Terrain] Pushed vertex = (x=%.2f, y=%.2f, z=%.2f, u=%.2f, v=%.2f, r=%.2f, g=%.2f, b=%.2f)\n", v2.position.x, v2.position.y, v2.position.z, v2.texture.x, v2.texture.y, v2.color.x, v2.color.y, v2.color.z );
			#else
			printf( "[Bitmap Terrain] Pushed vertex = (x=%.2f, y=%.2f, z=%.2f, u=%.2f, v=%.2f)\n", v2.position.x, v2.position.y, v2.position.z, v2.texture.x, v2.texture.y );
			#endif
			#endif

			lc_vector_push( tg->geometry, v2 );
			if( tri_strip ) lc_vector_push( tg->geometry, v3 );
		}

		if( tri_strip ) lc_vector_push( tg->geometry, v3 );
	}

	#ifdef LIBTERRAIN_DEBUG
	bench_mark_end( bm );
	bench_mark_report( bm );
	bench_mark_destroy( bm );
	printf( "[Bitmap Terrain] Terrain generation complete (Vertex Count = %zd)\n", lc_vector_size(tg->geometry) );
	#endif
}


void bitmap_terrain_gen_construct_triangles( bitmap_terrain_gen_t* tg )
{
}

void bitmap_terrain_gen_construct_triangle_strip( bitmap_terrain_gen_t* tg )
{
}

uint32_t bitmap_terrain_gen_element_width( const bitmap_terrain_gen_t* tg )
{
	return tg->element_width;
}

uint32_t bitmap_terrain_gen_element_length( const bitmap_terrain_gen_t* tg )
{
	return tg->element_length;
}

float bitmap_terrain_gen_unit_width( const bitmap_terrain_gen_t* tg )
{
	return tg->unit_width;
}

float bitmap_terrain_gen_unit_length( const bitmap_terrain_gen_t* tg )
{
	return tg->unit_length;
}

float bitmap_terrain_gen_width( const bitmap_terrain_gen_t* tg )
{
	return tg->terrain_width;
}

float bitmap_terrain_gen_length( const bitmap_terrain_gen_t* tg )
{
	return tg->terrain_length;
}

float bitmap_terrain_gen_max_height( const bitmap_terrain_gen_t* tg )
{
	return tg->terrain_max_height;
}

float bitmap_terrain_gen_min_height( const bitmap_terrain_gen_t* tg )
{
	return tg->terrain_min_height;
}

terrain_vertex_t* bitmap_terrain_gen_geometry( const bitmap_terrain_gen_t* tg )
{
	return tg->geometry;
}

size_t bitmap_terrain_gen_geometry_size( const bitmap_terrain_gen_t* tg )
{
	return lc_vector_size( tg->geometry );
}

#ifdef TERRAIN_VERTEX_HAS_COLOR
vec3_t terrain_color_ramp( const bitmap_terrain_gen_t* tg, float height )
{
	float true_height   = tg->terrain_max_height - tg->terrain_min_height;
	float terrain_level = true_height * 0.10f;
	assert( height >= tg->terrain_min_height );
	vec3_t result = VEC3( 1.0f, 1.0f, 1.0f );

	if( height < (tg->terrain_min_height + 2.5 * terrain_level) )
	{
		result.x = 0.1f;
		result.y = 0.1f;
		result.z = 0.9f;
	}
	else if( height <  tg->terrain_min_height + 3 * terrain_level )
	{
		result.x = 0.7f;
		result.y = 0.7f;
		result.z = 0.3f;
	}
	else if( height <  tg->terrain_min_height + 4 * terrain_level )
	{
		result.x = 0.3f;
		result.y = 0.25f;
		result.z = 0.15f;
	}
	else if( height <  tg->terrain_min_height + 5 * terrain_level )
	{
		result.x = 0.3f;
		result.y = 0.6f;
		result.z = 0.3f;
	}
	else if( height <  tg->terrain_min_height + 6 * terrain_level )
	{
		result.x = 0.3f;
		result.y = 0.85f;
		result.z = 0.3f;
	}
	else if( height <  tg->terrain_min_height + 9 * terrain_level )
	{
		result.x = 0.0;
		result.y = 0.8f;
		result.z = 0.0f;
	}

	return result;
}
#endif
