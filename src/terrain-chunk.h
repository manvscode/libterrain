/*
 * Copyright (C) 2013 Joseph A. Marrero.   http://www.manvscode.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef _TERRAIN_CHUNK_H_
#define _TERRAIN_CHUNK_H_

#include <string.h>
#include "terrain.h"

struct terrain_chunk {
	const terrain_cell_t* cells[ 9 ];
	uint16_t alphamaps[ TERRAIN_MAX_TEXTURES - 1 ];
};
			
static inline void terrain_chunk_initialize( terrain_chunk_t* chunk )
{
	memset( chunk->cells, 0, sizeof(terrain_cell_t*) * 9 );
	memset( chunk->alphamaps, 0, sizeof(uint16_t) * (TERRAIN_MAX_TEXTURES - 1) );
}


#endif /* _TERRAIN_CHUNK_H_ */
