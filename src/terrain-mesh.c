/*
 * Copyright (C) 2013 Joseph A. Marrero.   http://www.manvscode.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include "terrain.h"

struct terrain_mesh {
	/* unit width: the smallest width between two vertices along x-axis */
	float unit_width;
 	/* unit length: the smallest length between two vertices along z-axis */
	float unit_length;
	/* element width: the number of possible indices along x-axis */
	uint32_t element_width;
	/* element length: the number of possible indices along z-axis */
	uint32_t element_length;

	float width;
	float length;
	float height;
	bool is_tri_strip;
	size_t vertex_count;
	terrain_vertex_t* vertices;
};

terrain_mesh_t* terrain_mesh_create( void )
{
	terrain_mesh_t* mesh = (terrain_mesh_t*) malloc( sizeof(terrain_mesh_t) );

	if( mesh )
	{
		mesh->unit_width     = 0.0f;
		mesh->unit_length    = 0.0f;
		mesh->element_width  = 0;
		mesh->element_length = 0;
		mesh->width          = 0.0f;
		mesh->length         = 0.0f;
		mesh->height         = 0.0f;
		mesh->is_tri_strip   = false;
		mesh->vertex_count   = 0;
		mesh->vertices       = NULL;
	}

	return mesh;
}

void terrain_mesh_destroy( terrain_mesh_t* tm )
{
	assert( tm );
	if( tm->vertices ) free( tm->vertices );
	free( tm );
}

void terrain_mesh_set_geometry( terrain_mesh_t* mesh, const terrain_vertex_t* vertices, size_t count, uint32_t element_width, uint32_t element_length, bool is_tri_strip )
{
	assert( mesh );
	assert( vertices );

	if( mesh->vertices )
	{
		free( mesh->vertices );
		mesh->vertex_count = 0;
	}

	size_t size = sizeof(terrain_vertex_t) * count;
	mesh->vertices = (terrain_vertex_t*) malloc( size );

	if( mesh->vertices )
	{
		// TODO: move the copy into the loop below for possible optimization
		memcpy( mesh->vertices, vertices, size );
		mesh->vertex_count = count;


		float min_width  = 0.0f;
		float max_width  = 0.0f;
		float min_length = 0.0f;
		float max_length = 0.0f;
		float max_height = 0.0f;
		float min_height = 0.0f;

		for( size_t i = 0; i < mesh->vertex_count; i++ )
		{
			terrain_vertex_t* vert = &mesh->vertices[ i ];

			if( vert->position.x < min_width )  min_width  = vert->position.x;
			if( vert->position.x > max_width )  max_width  = vert->position.x;
			if( vert->position.x < min_length ) min_length = vert->position.z;
			if( vert->position.x > max_length ) max_length = vert->position.z;
			if( vert->position.y < min_height ) min_height = vert->position.y;
			if( vert->position.y > max_height ) max_height = vert->position.y;
		}

		mesh->width          = (max_width - min_width);
		mesh->length         = (max_length - min_length);
		mesh->height         = (max_height - min_height);
		mesh->element_width  = element_width;
		mesh->element_length = element_length;
		mesh->unit_width     = mesh->width / mesh->element_width;
		mesh->unit_length    = mesh->length / mesh->element_length;
		mesh->is_tri_strip   = is_tri_strip;
	}
	else
	{
		mesh->width        = 0.0f;
		mesh->length       = 0.0f;
		mesh->height       = 0.0f;
		mesh->vertices     = NULL;
		mesh->vertex_count = 0;
	}
}

float terrain_mesh_width( const terrain_mesh_t* mesh )
{
	assert( mesh );
	return mesh->width;
}

float terrain_mesh_length( const terrain_mesh_t* mesh )
{
	assert( mesh );
	return mesh->length;
}

float terrain_mesh_height( const terrain_mesh_t* mesh )
{
	assert( mesh );
	return mesh->height;
}

float terrain_mesh_unit_width( const terrain_mesh_t* mesh )
{
	assert( mesh );
	return mesh->unit_width;
}

float terrain_mesh_unit_length( const terrain_mesh_t* mesh )
{
	assert( mesh );
	return mesh->unit_length;
}

uint32_t terrain_mesh_element_width( const terrain_mesh_t* mesh )
{
	assert( mesh );
	return mesh->element_width;
}

uint32_t terrain_mesh_element_length( const terrain_mesh_t* mesh )
{
	assert( mesh );
	return mesh->element_length;
}

terrain_vertex_t* terrain_mesh_vertices( const terrain_mesh_t* mesh )
{
	assert( mesh );
	return mesh->vertices;
}

size_t terrain_mesh_vertex_count( const terrain_mesh_t* mesh )
{
	assert( mesh );
	return mesh->vertex_count;
}

terrain_vertex_t* terrain_mesh_vertex( const terrain_mesh_t* mesh, int32_t x, int32_t y )
{
	assert( mesh );
	//return &mesh->vertices[ (y % mesh->element_length) * mesh->element_width + (x % mesh->element_width) ];
	return &mesh->vertices[ y * mesh->element_width + x ];
}

bool terrain_mesh_is_tri_strip( const terrain_mesh_t* mesh )
{
	assert( mesh );
	return mesh->is_tri_strip;
}

const terrain_mesh_t* terrain_mesh_load( FILE* file )
{
	terrain_mesh_t* result = terrain_mesh_create( );
	// TODO: Need to implement this!
	return result;
}

bool terrain_mesh_save( const terrain_mesh_t* mesh, FILE* file )
{
	bool result = false;
	// TODO: Need to implement this!
	return result;
}

