/*
 * Copyright (C) 2013 Joseph A. Marrero.   http://www.manvscode.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef _TERRAIN_CELL_H_
#define _TERRAIN_CELL_H_

#include <stdint.h>
#include "terrain.h"

terrain_cell_t* terrain_cell_create     ( size_t vertex_count );
void            terrain_cell_destroy    ( terrain_cell_t* tc );
void            terrain_cell_push_index ( terrain_cell_t* cell, uint32_t index );
void            terrain_cell_set_center ( terrain_cell_t* cell, float x, float z );

struct terrain_cell {
	vec3_t center;
	uint32_t* indices;
	//uint32_t layers[ MAX_TERRAIN_TEXTURES ];
};

#endif /* _TERRAIN_CELL_H_ */
