/*
 * Copyright (C) 2013 Joseph A. Marrero.   http://www.manvscode.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef _TERRAIN_H_
#define _TERRAIN_H_
#include <stddef.h>
#if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L)
# include <stdbool.h>
# include <stdint.h>
# ifdef __restrict
#  undef __restrict
#  define __restrict restrict
# endif
# ifdef __inline
#  undef __inline
#  define __inline inline
# endif
#else /* No C99 */
# define bool int
# define true 1
# define false 0
# ifdef __restrict
#  undef __restrict
#  define __restrict
# endif
# ifdef __inline
#  undef __inline
#  define __inline
# endif
#endif
#ifdef __cplusplus
extern "C" {
#endif
#include <m3d/mathematics.h>
#include <m3d/vec2.h>
#include <m3d/vec3.h>

/*
 *  Terrain Vertex Data
 */
typedef struct terrain_vertex {
	vec3_t position;
	vec3_t normal;
	vec2_t texture;
	#ifdef TERRAIN_VERTEX_HAS_COLOR
	vec3_t color;
	#endif
} terrain_vertex_t;

typedef enum terrain_primitive_type {
	TERRAIN_PRIMITIVE_POINTS         = 0x0,
	TERRAIN_PRIMITIVE_TRIANGLES      = 0x1,
	TERRAIN_PRIMITIVE_TRIANGLE_STRIP = 0x2,
} terrain_primitive_type_t;

#define TERRAIN_MAX_TEXTURES           2
#define TERRAIN_ALPHA_MAP_SIZE         32

/*
 * Bitmap Terrain Generator
 *
 * Generate a terrain height field from bitmap data.
 */
struct bitmap_terrain_gen;
typedef struct bitmap_terrain_gen bitmap_terrain_gen_t;

bitmap_terrain_gen_t* bitmap_terrain_gen_create         ( float terrain_width, float terrain_length, float terrain_min_height, float terrain_max_height );
void                  bitmap_terrain_gen_destroy        ( bitmap_terrain_gen_t* tg );
void                  bitmap_terrain_gen_generate       ( bitmap_terrain_gen_t* tg, uint32_t width, uint32_t height,
                                                          uint8_t bytes_per_pixel, const void *bitmap, float scale_uvs, bool tri_strip );
uint32_t              bitmap_terrain_gen_element_width  ( const bitmap_terrain_gen_t* tg );
uint32_t              bitmap_terrain_gen_element_length ( const bitmap_terrain_gen_t* tg );
float                 bitmap_terrain_gen_unit_width     ( const bitmap_terrain_gen_t* tg );
float                 bitmap_terrain_gen_unit_length    ( const bitmap_terrain_gen_t* tg );
float                 bitmap_terrain_gen_width          ( const bitmap_terrain_gen_t* tg );
float                 bitmap_terrain_gen_length         ( const bitmap_terrain_gen_t* tg );
float                 bitmap_terrain_gen_max_height     ( const bitmap_terrain_gen_t* tg );
float                 bitmap_terrain_gen_min_height     ( const bitmap_terrain_gen_t* tg );
terrain_vertex_t*     bitmap_terrain_gen_geometry       ( const bitmap_terrain_gen_t* tg );
size_t                bitmap_terrain_gen_geometry_size  ( const bitmap_terrain_gen_t* tg );

/*
 * Fractal Terrain Generator
 *
 * Generate a terrain height field from fractal-based
 * algorithms.
 */
typedef enum fractal_terrain_algorithm {
	FTG_RANDOMIZED_PUSH,
	FTG_DIAMOND_SQUARE,
} fractal_terrain_algorithm_t;

struct fractal_terrain_gen;
typedef struct fractal_terrain_gen fractal_terrain_gen_t;

fractal_terrain_gen_t* fractal_terrain_gen_create        ( uint32_t width, uint32_t length, float min_height, float max_height, float average_push, uint32_t seed );
void                   fractal_terrain_gen_destroy       ( fractal_terrain_gen_t* ftg );
void                   fractal_terrain_gen_generate      ( fractal_terrain_gen_t* ftg, fractal_terrain_algorithm_t alg, float scale_uvs, bool tri_strip );
uint32_t               fractal_terrain_gen_element_width ( const fractal_terrain_gen_t* ftg );
uint32_t               fractal_terrain_gen_element_length( const fractal_terrain_gen_t* ftg );
float                  fractal_terrain_gen_unit_width    ( const fractal_terrain_gen_t* ftg );
float                  fractal_terrain_gen_unit_length   ( const fractal_terrain_gen_t* ftg );
float                  fractal_terrain_gen_width         ( const fractal_terrain_gen_t* ftg );
float                  fractal_terrain_gen_length        ( const fractal_terrain_gen_t* ftg );
float                  fractal_terrain_gen_max_height    ( const fractal_terrain_gen_t* ftg );
float                  fractal_terrain_gen_min_height    ( const fractal_terrain_gen_t* ftg );
void                   fractal_terrain_gen_seed_height   ( fractal_terrain_gen_t* ftg, uint32_t x, uint32_t z, float height );
terrain_vertex_t*      fractal_terrain_gen_geometry      ( const fractal_terrain_gen_t* ftg );
size_t                 fractal_terrain_gen_geometry_size ( const fractal_terrain_gen_t* ftg );

/*
 * Terrain Mesh
 *
 * A terrain mesh is a container for terrain vertices.
 */
struct terrain_mesh;
typedef struct terrain_mesh terrain_mesh_t;

terrain_mesh_t*   terrain_mesh_create         ( void );
void              terrain_mesh_destroy        ( terrain_mesh_t* mesh );
void              terrain_mesh_set_geometry   ( terrain_mesh_t* mesh, const terrain_vertex_t* vertices, size_t count, uint32_t element_width, uint32_t element_length, bool is_tri_strip );
float             terrain_mesh_width          ( const terrain_mesh_t* mesh );
float             terrain_mesh_length         ( const terrain_mesh_t* mesh );
float             terrain_mesh_height         ( const terrain_mesh_t* mesh );
float             terrain_mesh_unit_width     ( const terrain_mesh_t* mesh );
float             terrain_mesh_unit_length    ( const terrain_mesh_t* mesh );
uint32_t          terrain_mesh_element_width  ( const terrain_mesh_t* mesh );
uint32_t          terrain_mesh_element_length ( const terrain_mesh_t* mesh );
terrain_vertex_t* terrain_mesh_vertices       ( const terrain_mesh_t* mesh );
size_t            terrain_mesh_vertex_count   ( const terrain_mesh_t* mesh );
terrain_vertex_t* terrain_mesh_vertex         ( const terrain_mesh_t* mesh, int32_t x, int32_t y );
bool              terrain_mesh_is_tri_strip   ( const terrain_mesh_t* mesh );
//terrain_mesh_t*   terrain_mesh_load           ( FILE* file ); // TODO: Need to implement this!
//bool              terrain_mesh_save           ( const terrain_mesh_t* mesh, FILE* file ); // TODO: Need to implement this!

/*
 * Terrain Cell
 *
 * A terrain cell is a container for indices into the
 * terrain mesh.
 */
struct terrain_cell;
typedef struct terrain_cell terrain_cell_t;

const vec3_t*   terrain_cell_center             ( const terrain_cell_t* cell );
const uint32_t* terrain_cell_vertex_indices     ( const terrain_cell_t* cell );
size_t          terrain_cell_vertex_index_count ( const terrain_cell_t* cell );

/*
 * Terrain Grid
 */
struct terrain_grid;
typedef struct terrain_grid terrain_grid_t;

terrain_grid_t*       terrain_grid_create       ( const terrain_mesh_t* mesh, uint32_t xcells, uint32_t ycells, bool uvs_mapped_to_grid, float scale_uvs );
void                  terrain_grid_destroy      ( terrain_grid_t* tg );
const terrain_mesh_t* terrain_grid_mesh         ( const terrain_grid_t* tg );
uint32_t              terrain_grid_xcells       ( const terrain_grid_t* tg );
uint32_t              terrain_grid_ycells       ( const terrain_grid_t* tg );
float                 terrain_grid_cell_width   ( const terrain_grid_t* tg );
float                 terrain_grid_cell_length  ( const terrain_grid_t* tg );
const terrain_cell_t* terrain_grid_cell         ( const terrain_grid_t* tg, uint32_t x, uint32_t y );

/*
 * Terrain Chunk
 *
 * A terrain chunk is grouping of 3x3 terrain cells. It
 * allows for multitexturing the transitions of tiles.
 */
struct terrain_chunk;
typedef struct terrain_chunk terrain_chunk_t;

uint16_t terrain_chunk_alphamap    ( const terrain_chunk_t* chunk, uint8_t layer );
void     terrain_chunk_set_alphmap ( terrain_chunk_t* chunk, uint8_t layer, uint16_t tilebits );

/*
 * Texture Splatted Terrain
 */
struct texture_splatted_terrain;
typedef struct texture_splatted_terrain texture_splatted_terrain_t;

texture_splatted_terrain_t* texture_splatted_terrain_create      ( const terrain_mesh_t* mesh, uint32_t xchunks, uint32_t ychunks );
void                        texture_splatted_terrain_destroy     ( texture_splatted_terrain_t* ts );
uint32_t                    texture_splatted_terrain_xchunks     ( const texture_splatted_terrain_t* ts );
uint32_t                    texture_splatted_terrain_ychunks     ( const texture_splatted_terrain_t* ts );
const terrain_grid_t*       texture_splatted_terrain_grid        ( const texture_splatted_terrain_t* ts );
terrain_chunk_t*            texture_splatted_terrain_chunk       ( texture_splatted_terrain_t* ts, uint32_t x, uint32_t y );
uint32_t                    texture_splatted_terrain_texture     ( const texture_splatted_terrain_t* ts, uint8_t layer );
void                        texture_splatted_terrain_set_texture ( texture_splatted_terrain_t* ts, uint8_t layer, uint32_t texture );


/*
 * Tile Blending Alphamaps
 */
struct terrain_alphamap;
typedef struct terrain_alphamap terrain_alphamap_t;

terrain_alphamap_t* terrain_alphamap_create_default ( void );
terrain_alphamap_t* terrain_alphamap_create         ( uint16_t width, uint16_t height, uint8_t channels, bool noise, float alpha_bias );
terrain_alphamap_t* terrain_alphamap_from_file      ( const char* filename );
void                terrain_alphamap_destroy        ( terrain_alphamap_t* alphamap );
uint16_t            terrain_alphamap_width          ( const terrain_alphamap_t* am );
uint16_t            terrain_alphamap_height         ( const terrain_alphamap_t* am );
const void*         terrain_alphamap_bitmap         ( const terrain_alphamap_t* am, uint16_t tilebits );
bool                terrain_alphamap_save_file      ( const terrain_alphamap_t* am, const char* filename );


#ifdef __cplusplus
} /* extern C Linkage */
#endif
#endif /* _TERRAIN_H_ */
